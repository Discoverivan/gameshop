package ru.discoverivan.gameshop.validation;

import java.io.Serializable;
import java.util.Map;

public class ValidationResult implements Serializable {
    private Boolean success;
    private Map<String, String> fieldErrors;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Map<String, String> getFieldErrors() {
        return fieldErrors;
    }

    public void setFieldErrors(Map<String, String> fieldErrors) {
        this.fieldErrors = fieldErrors;
    }
}
