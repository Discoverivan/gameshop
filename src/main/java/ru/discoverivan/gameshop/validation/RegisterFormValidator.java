package ru.discoverivan.gameshop.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import ru.discoverivan.gameshop.db.dao.CustomerDAO;
import ru.discoverivan.gameshop.db.dao.UserDAO;
import ru.discoverivan.gameshop.db.entity.Customer;
import ru.discoverivan.gameshop.validation.RegisterForm;
import ru.discoverivan.gameshop.validation.ValidationResult;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

@Component
public class RegisterFormValidator {

    private UserDAO userDAO;
    private CustomerDAO customerDAO;

    private Pattern passwordPattern = Pattern.compile("^[_a-zA-Z0-9]{3,30}$");
    private Pattern usernamePattern = Pattern.compile("^[a-zA-Z0-9]{2,30}$");

    public ValidationResult validate(RegisterForm form){
        ValidationResult validationResult = new ValidationResult();
        Map<String, String> errors = new HashMap<>();

        validationResult.setSuccess(true);
        if (!usernamePattern.matcher(form.getUsername()).matches()){
            validationResult.setSuccess(false);
            errors.put("username", "Имя пользователя должно быть из 2-30 символов и состоять из латинских букв и цифр.");
        }
        if (userDAO.findByUsername(form.getUsername()) != null){
            validationResult.setSuccess(false);
            errors.put("username", "Имя пользователя уже используется.");
        }
        if (!passwordPattern.matcher(form.getPassword()).matches()){
            validationResult.setSuccess(false);
            errors.put("password", "Пароль должен быть из 3-30 символов и состоять из латинских букв и цифр, а также знака подчеркивания.");
        }
        if (!form.getPassword().equals(form.getPasswordConfirm())){
            validationResult.setSuccess(false);
            errors.put("passwordConfirm", "Пароли должны совпадать.");
        }
        if (customerDAO.findByEmail(form.getEmail()) != null){
            validationResult.setSuccess(false);
            errors.put("email", "Адрес электронной почты уже используется.");
        }

        validationResult.setFieldErrors(errors);
        return validationResult;
    }

    @Autowired
    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Autowired
    public void setCustomerDAO(CustomerDAO customerDAO) {
        this.customerDAO = customerDAO;
    }
}
