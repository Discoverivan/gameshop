package ru.discoverivan.gameshop.db;

import java.sql.*;

public class JDBCTemplate implements AutoCloseable {

    private Connection connection;

    public JDBCTemplate(String url, String user, String password) {
        try {
            Class.forName("org.h2.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            connection = DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public JDBCTemplate() {
        this("jdbc:h2:~/database","root", "root");
    }

    public ResultSet query(String q, Object ... params) {
        try {
            PreparedStatement preparedStatement = createPreparedStatement(q, params);
            if (preparedStatement != null) {
                preparedStatement.execute();
                return preparedStatement.getResultSet();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ResultSet query(String q) {
        try {
            Statement statement = connection.createStatement();
            statement.execute(q);
            return statement.getResultSet();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean execute(String q, Object ... params){
        try (PreparedStatement preparedStatement = createPreparedStatement(q, params)) {
            if (preparedStatement != null) {
                return preparedStatement.execute();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean execute(String q) {
        try (Statement statement = connection.createStatement()) {
            return statement.execute(q);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public int update(String q, Object ... params){
        try (PreparedStatement preparedStatement = createPreparedStatement(q, params)){
            if (preparedStatement != null) {
                return preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int update(String q){
        try (Statement statement = connection.createStatement()) {
            return statement.executeUpdate(q);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    private PreparedStatement createPreparedStatement(String q, Object ... params) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(q);
            int i = 1;
            for (Object param : params) {
                preparedStatement.setObject(i,param);
                i++;
            }
            return preparedStatement;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void close() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
