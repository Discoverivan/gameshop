package ru.discoverivan.gameshop.db.dao;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.discoverivan.gameshop.db.HibernateUtil;
import ru.discoverivan.gameshop.db.entity.Genre;
import ru.discoverivan.gameshop.db.entity.Platform;

import java.util.List;

@Repository
public class H2PlatformDAO implements PlatformDAO {

    H2EntityDAO<Platform> entityDAO;

    @Autowired
    public void setEntityDAO(H2EntityDAO<Platform> entityDAO) {
        this.entityDAO = entityDAO;
    }

    @Override
    public void add(Platform platform) {
        entityDAO.add(platform);
    }

    @Override
    public Platform get(int id) {
        Session session= HibernateUtil.getSession();
        session.beginTransaction();
        Query query = session.createQuery("from Platform where id=:id");
        query.setParameter("id", id);
        Platform platform= (Platform) query.uniqueResult();
        session.getTransaction().commit();
        session.close();
        return platform;
    }

    @Override
    public List<Platform> getAll() {
        Session session= HibernateUtil.getSession();
        session.beginTransaction();
        List<Platform> platforms = (List<Platform>) session.createQuery("from Platform").list();
        session.getTransaction().commit();
        session.close();
        return platforms;
    }

    @Override
    public void delete(int id) {
        Session session=HibernateUtil.getSession();
        session.beginTransaction();
        Query query=session.createQuery("from Platform where id=:id");
        query.setParameter("id",id);
        Platform platform= (Platform) query.uniqueResult();
        session.delete(platform);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void update(Platform updatedPlatform) {
        entityDAO.update(updatedPlatform);
    }

    @Override
    public void clear() {
        Session session=HibernateUtil.getSession();
        session.beginTransaction();
        session.createQuery("delete from Platform").executeUpdate();
        session.getTransaction().commit();
        session.close();
    }
}
