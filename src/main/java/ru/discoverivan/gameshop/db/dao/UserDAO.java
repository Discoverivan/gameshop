package ru.discoverivan.gameshop.db.dao;

import ru.discoverivan.gameshop.db.entity.User;

import java.util.List;

public interface UserDAO {
    void add(User user);

    User get(int id);

    User findByUsername(String username);

    List<User> getAll();

    void delete(int id);

    void update(User updatedUser);

    void clear();
}
