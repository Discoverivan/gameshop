package ru.discoverivan.gameshop.db.dao;

import ru.discoverivan.gameshop.db.entity.Game;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class StaticListGameDAO implements GameDAO {
    private static List<Game> games = new ArrayList<>();
    private static int counter = 0;

    @Override
    public void add(Game game){
        game.setId(counter++);
        games.add(game);
    }

    @Override
    public Game get(int id){
        for (Game game : games) {
            if (game.getId() == id){
                return game;
            }
        }
        throw new NoSuchElementException();
    }

    @Override
    public  List<Game> getAll(){
        return games;
    }

    @Override
    public List<Game> getOrderedList(String fieldName, int limit) {
        return null;
    }

    @Override
    public List<Game> selectListByField(String fieldName, String fieldValue, int limit) {
        return null;
    }

    @Override
    public  void delete(int id) {
        games.remove(get(id));
    }

    @Override
    public void update(Game updatedGame) {

    }

    @Override
    public int count() {
        return 0;
    }

    @Override
    public  void clear() {
        counter = 0;
        games.clear();
    }
}
