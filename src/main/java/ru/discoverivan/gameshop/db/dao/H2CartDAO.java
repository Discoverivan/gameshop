package ru.discoverivan.gameshop.db.dao;

import ru.discoverivan.gameshop.db.entity.additional.Cart;
import ru.discoverivan.gameshop.db.entity.Language;

import java.util.List;

public class H2CartDAO implements CartDAO {

    H2EntityDAO<Language> entityDAO;

    @Override
    public void add(Cart cart) {

    }

    @Override
    public Cart get(int id) {
        return null;
    }

    @Override
    public List<Cart> getAll() {
        return null;
    }

    @Override
    public void delete(int id) {

    }

    @Override
    public void update(Cart updatedCart) {

    }

    @Override
    public void clear() {

    }
}
