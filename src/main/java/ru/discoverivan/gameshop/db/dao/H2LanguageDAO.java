package ru.discoverivan.gameshop.db.dao;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.discoverivan.gameshop.db.HibernateUtil;
import ru.discoverivan.gameshop.db.entity.Language;

import java.util.List;

@Repository
public class H2LanguageDAO implements LanguageDAO {

    H2EntityDAO<Language> entityDAO;

    @Autowired
    public void setEntityDAO(H2EntityDAO<Language> entityDAO) {
        this.entityDAO = entityDAO;
    }

    @Override
    public void add(Language language) {
        entityDAO.add(language);
    }

    @Override
    public Language get(int id) {
        Session session= HibernateUtil.getSession();
        session.beginTransaction();
        Query query = session.createQuery("from Language where id=:id");
        query.setParameter("id", id);
        Language language= (Language) query.uniqueResult();
        session.getTransaction().commit();
        session.close();
        return language;
    }

    @Override
    public List<Language> getAll() {
        Session session= HibernateUtil.getSession();
        session.beginTransaction();
        List<Language> languages = (List<Language>) session.createQuery("from Language").list();
        session.getTransaction().commit();
        session.close();
        return languages;
    }

    @Override
    public void delete(int id) {
        Session session=HibernateUtil.getSession();
        session.beginTransaction();
        Query query=session.createQuery("from Language where id=:id");
        query.setParameter("id",id);
        Language language= (Language) query.uniqueResult();
        session.delete(language);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void update(Language updatedLanguage) {
        entityDAO.update(updatedLanguage);
    }

    @Override
    public void clear() {
        Session session=HibernateUtil.getSession();
        session.beginTransaction();
        session.createQuery("delete from Language").executeUpdate();
        session.getTransaction().commit();
        session.close();
    }
}
