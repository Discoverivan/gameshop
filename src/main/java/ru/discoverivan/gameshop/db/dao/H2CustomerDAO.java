package ru.discoverivan.gameshop.db.dao;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.discoverivan.gameshop.db.HibernateUtil;
import ru.discoverivan.gameshop.db.entity.Customer;
import ru.discoverivan.gameshop.db.entity.User;

import java.util.List;

@Repository
public class H2CustomerDAO implements CustomerDAO {

    private H2EntityDAO<Customer> entityDAO;

    @Autowired
    public void setEntityDAO(H2EntityDAO<Customer> entityDAO) {
        this.entityDAO = entityDAO;
    }

    @Override
    public void add(Customer customer) {
        entityDAO.add(customer);
    }

    @Override
    public Customer get(int id) {
        Session session= HibernateUtil.getSession();
        session.beginTransaction();
        Query query = session.createQuery("from Customer where id=:id");
        query.setParameter("id", id);
        Customer customer= (Customer) query.uniqueResult();
        session.getTransaction().commit();
        session.close();
        return customer;
    }

    @Override
    public Customer findByEmail(String email) {
        Session session= HibernateUtil.getSession();
        session.beginTransaction();
        Query query = session.createQuery("from Customer where email=:email");
        query.setParameter("email", email);
        Customer customer= (Customer) query.uniqueResult();
        session.getTransaction().commit();
        session.close();
        return customer;
    }

    @Override
    public List<Customer> getAll() {
        Session session= HibernateUtil.getSession();
        session.beginTransaction();
        List<Customer> customers = (List<Customer>) session.createQuery("from Customer").list();
        session.getTransaction().commit();
        session.close();
        return customers;
    }

    @Override
    public void delete(int id) {
        Session session=HibernateUtil.getSession();
        session.beginTransaction();
        Query query=session.createQuery("from Customer where id=:id");
        query.setParameter("id",id);
        Customer customer= (Customer) query.uniqueResult();
        session.delete(customer);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void update(Customer updatedCustomer) {
        entityDAO.update(updatedCustomer);
    }

    @Override
    public int count() {
        Session session= HibernateUtil.getSession();
        session.beginTransaction();
        Long count = (Long) session.createQuery("select count(*) from Customer ").uniqueResult();
        session.getTransaction().commit();
        session.close();
        return count.intValue();
    }

    @Override
    public void clear() {
        Session session=HibernateUtil.getSession();
        session.beginTransaction();
        session.createQuery("delete from Customer").executeUpdate();
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public Customer findByUsername(String username) {
        Session session= HibernateUtil.getSession();
        session.beginTransaction();
        Query query = session.createQuery("from Customer where username=:username");
        query.setParameter("username", username);
        Customer customer= (Customer) query.uniqueResult();
        session.getTransaction().commit();
        session.close();
        return customer;
    }
}
