package ru.discoverivan.gameshop.db.dao;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.discoverivan.gameshop.db.HibernateUtil;
import ru.discoverivan.gameshop.db.entity.Customer;
import ru.discoverivan.gameshop.db.entity.User;

import java.util.List;

@Repository
public class H2UserDAO implements UserDAO {

    private H2EntityDAO<User> entityDAO;

    @Autowired
    public void setEntityDAO(H2EntityDAO<User> entityDAO) {
        this.entityDAO = entityDAO;
    }

    @Override
    public void add(User user) {
        entityDAO.add(user);
    }

    @Override
    public User get(int id) {
        Session session= HibernateUtil.getSession();
        session.beginTransaction();
        Query query = session.createQuery("from User where id=:id");
        query.setParameter("id", id);
        User user= (User) query.uniqueResult();
        session.getTransaction().commit();
        session.close();
        return user;
    }

    @Override
    public User findByUsername(String username) {
        Session session= HibernateUtil.getSession();
        session.beginTransaction();
        Query query = session.createQuery("from User where username=:username");
        query.setParameter("username", username);
        User user= (User) query.uniqueResult();
        session.getTransaction().commit();
        session.close();
        return user;
    }

    @Override
    public List<User> getAll() {
        Session session= HibernateUtil.getSession();
        session.beginTransaction();
        List<User> users = (List<User>) session.createQuery("from User").list();
        session.getTransaction().commit();
        session.close();
        return users;
    }

    @Override
    public void delete(int id) {
        Session session=HibernateUtil.getSession();
        session.beginTransaction();
        Query query=session.createQuery("from User where id=:id");
        query.setParameter("id",id);
        User user= (User) query.uniqueResult();
        session.delete(user);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void update(User updatedUser) {
        entityDAO.update(updatedUser);
    }

    @Override
    public void clear() {
        Session session=HibernateUtil.getSession();
        session.beginTransaction();
        session.createQuery("delete from User").executeUpdate();
        session.getTransaction().commit();
        session.close();
    }
}
