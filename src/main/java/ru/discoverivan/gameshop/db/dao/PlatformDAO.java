package ru.discoverivan.gameshop.db.dao;

import ru.discoverivan.gameshop.db.entity.Language;
import ru.discoverivan.gameshop.db.entity.Platform;

import java.util.List;

public interface PlatformDAO {
    void add(Platform platform);

    Platform get(int id);

    List<Platform> getAll();

    void delete(int id);

    void update(Platform updatedPlatform);

    void clear();
}
