package ru.discoverivan.gameshop.db.dao;

import ru.discoverivan.gameshop.db.entity.Order;

import java.util.Date;
import java.util.List;

public interface OrderDAO {
    void add(Order order);

    Order get(int id);

    List<Order> getAll();

    List<Order> getOrdersInTimeInterval(Date from, Date to);

    void delete(int id);

    void update(Order updatedOrder);

    int count();

    void clear();
}
