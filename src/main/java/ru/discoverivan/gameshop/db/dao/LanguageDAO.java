package ru.discoverivan.gameshop.db.dao;

import ru.discoverivan.gameshop.db.entity.Game;
import ru.discoverivan.gameshop.db.entity.Language;

import java.util.List;

public interface LanguageDAO {
    void add(Language language);

    Language get(int id);

    List<Language> getAll();

    void delete(int id);

    void update(Language updatedLanguage);

    void clear();
}
