package ru.discoverivan.gameshop.db.dao;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.discoverivan.gameshop.db.HibernateUtil;
import ru.discoverivan.gameshop.db.entity.Game;
import ru.discoverivan.gameshop.db.entity.Order;
import ru.discoverivan.gameshop.db.entity.Product;

import java.util.List;

@Repository
public class H2ProductDAO implements ProductDAO {

    private H2EntityDAO<Product> entityDAO;

    @Autowired
    public void setEntityDAO(H2EntityDAO<Product> entityDAO) {
        this.entityDAO = entityDAO;
    }

    @Override
    public void add(Product product) {
        entityDAO.add(product);
    }

    @Override
    public Product get(int id) {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        Query query = session.createQuery("from Product where id=:id");
        query.setParameter("id", id);
        Product product = (Product) query.uniqueResult();
        session.getTransaction().commit();
        session.close();
        return product;
    }

    @Override
    public List<Product> getAllProductsByGame(Game game) {
        Session session= HibernateUtil.getSession();
        session.beginTransaction();
        Query query = session.createQuery("from Product where game=:game");
        query.setParameter("game", game);
        List<Product> products = query.list();
        session.getTransaction().commit();
        session.close();
        return products;
    }

    @Override
    public List<Product> getAll() {
        Session session= HibernateUtil.getSession();
        session.beginTransaction();
        List<Product> products = (List<Product>) session.createQuery("from Product").list();
        session.getTransaction().commit();
        session.close();
        return products;
    }

    @Override
    public void delete(int id) {
        Session session=HibernateUtil.getSession();
        session.beginTransaction();
        Product product= get(id);
        session.delete(product);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void update(Product updatedProduct) {
        entityDAO.update(updatedProduct);
    }

    @Override
    public int count() {
        Session session= HibernateUtil.getSession();
        session.beginTransaction();
        Long count = (Long) session.createQuery("select count(*) from Product").uniqueResult();
        session.getTransaction().commit();
        session.close();
        return count.intValue();
    }

    @Override
    public void clear() {
        Session session=HibernateUtil.getSession();
        session.beginTransaction();
        session.createQuery("delete from Product").executeUpdate();
        session.getTransaction().commit();
        session.close();
    }
}
