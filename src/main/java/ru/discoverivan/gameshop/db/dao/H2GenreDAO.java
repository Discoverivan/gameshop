package ru.discoverivan.gameshop.db.dao;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.discoverivan.gameshop.db.HibernateUtil;
import ru.discoverivan.gameshop.db.entity.Game;
import ru.discoverivan.gameshop.db.entity.Genre;
import ru.discoverivan.gameshop.db.entity.Language;

import java.util.List;

@Repository
public class H2GenreDAO implements GenreDAO {

    private H2EntityDAO<Genre> entityDAO;

    @Autowired
    public void setEntityDAO(H2EntityDAO<Genre> entityDAO) {
        this.entityDAO = entityDAO;
    }

    @Override
    public void add(Genre genre) {
        entityDAO.add(genre);
    }

    @Override
    public Genre get(int id) {
        Session session= HibernateUtil.getSession();
        session.beginTransaction();
        Query query = session.createQuery("from Genre where id=:id");
        query.setParameter("id", id);
        Genre genre= (Genre) query.uniqueResult();
        session.getTransaction().commit();
        session.close();
        return genre;
    }

    @Override
    public List<Genre> getAll() {
        Session session= HibernateUtil.getSession();
        session.beginTransaction();
        List<Genre> genres = (List<Genre>) session.createQuery("from Genre").list();
        session.getTransaction().commit();
        session.close();
        return genres;
    }

    @Override
    public void delete(int id) {
        Session session=HibernateUtil.getSession();
        session.beginTransaction();
        Query query=session.createQuery("from Genre where id=:id");
        query.setParameter("id",id);
        Genre genre= (Genre) query.uniqueResult();
        session.delete(genre);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void update(Genre updatedGenre) {
        entityDAO.update(updatedGenre);
    }

    @Override
    public void clear() {
        Session session=HibernateUtil.getSession();
        session.beginTransaction();
        session.createQuery("delete from Genre").executeUpdate();
        session.getTransaction().commit();
        session.close();
    }
}
