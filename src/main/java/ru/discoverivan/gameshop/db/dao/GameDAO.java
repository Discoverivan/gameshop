package ru.discoverivan.gameshop.db.dao;

import ru.discoverivan.gameshop.db.entity.Game;

import java.util.List;

public interface GameDAO {
    void add(Game game);

    Game get(int id);

    List<Game> getAll();

    List<Game> getOrderedList(String fieldName, int limit);

    List<Game> selectListByField(String fieldName, String fieldValue,  int limit);

    void delete(int id);

    void update(Game updatedGame);

    int count();

    void clear();
}
