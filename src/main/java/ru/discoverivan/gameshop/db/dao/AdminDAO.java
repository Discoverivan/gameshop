package ru.discoverivan.gameshop.db.dao;

import ru.discoverivan.gameshop.db.entity.Admin;

import java.util.List;

public interface AdminDAO {
    void add(Admin admin);

    Admin get(int id);

    Admin findByUsername(String username);

    List<Admin> getAll();

    void delete(int id);

    void update(Admin updatedAdmin);

    int count();

    void clear();
}
