package ru.discoverivan.gameshop.db.dao;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.discoverivan.gameshop.db.HibernateUtil;
import ru.discoverivan.gameshop.db.entity.Game;
import ru.discoverivan.gameshop.db.entity.Genre;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class H2GameDAO implements GameDAO {

    private H2EntityDAO<Game> entityDAO;

    @Override
    public String toString() {
        return "entitydao class: " + entityDAO.getClass().getName();
    }

    @Override
    public void add(Game game) {
        entityDAO.add(game);
    }

    @Override
    public Game get(int id) {
        Session session= HibernateUtil.getSession();

        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<Game> criteria = builder.createQuery(Game.class);
        Root<Game> root = criteria.from(Game.class);
        criteria.select(root);
        criteria.where(builder.equal( root.get("id") ,id));
        Game game = session.createQuery(criteria).uniqueResult();

        session.close();
        return game;
    }

    @Autowired
    public void setEntityDAO(H2EntityDAO<Game> entityDAO) {
        this.entityDAO = entityDAO;
    }

    @Override
    public List<Game> getAll() {
        Session session= HibernateUtil.getSession();
        session.beginTransaction();
        List<Game> games = (List<Game>) session.createQuery("from Game").list();
        session.getTransaction().commit();
        session.close();
        return games;
    }

    @Override
    public List<Game> getOrderedList(String fieldName, int limit) {
        Session session= HibernateUtil.getSession();
        session.beginTransaction();
        Query query = session.createQuery(String.format("from Game order by %s desc", fieldName));
        query.setMaxResults(limit);
        List<Game> games = (List<Game>) query.list();
        session.getTransaction().commit();
        session.close();
        return games;
    }

    @Override
    public List<Game> selectListByField(String fieldName, String fieldValue, int limit) {
        return null;
    }

    public List<Game> selectListByGenre(Genre genre, int limit){
        Session session= HibernateUtil.getSession();
        session.beginTransaction();
        Query query = session.createQuery("from Game game where :genre member of game.genres");
        query.setMaxResults(limit);
        query.setParameter("genre", genre);
        List<Game> games = (List<Game>) query.list();
        session.getTransaction().commit();
        session.close();
        return games;
    }

    @Override
    public void delete(int id) {
        Session session=HibernateUtil.getSession();
        session.beginTransaction();
        Query query=session.createQuery("from Game where id=:id");
        query.setParameter("id",id);
        Game game= (Game) query.uniqueResult();
        session.delete(game);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void update(Game updatedGame) {
        entityDAO.update(updatedGame);
    }

    @Override
    public int count() {
        Session session= HibernateUtil.getSession();
        session.beginTransaction();
        Long count = (Long) session.createQuery("select count(*) from Game").uniqueResult();
        session.getTransaction().commit();
        session.close();
        return count.intValue();
    }

    @Override
    public void clear() {
        List<Game> games = getAll();
        for (Game game : games) {
            delete(game.getId());
        }
    }
}
