package ru.discoverivan.gameshop.db.dao;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.discoverivan.gameshop.db.HibernateUtil;
import ru.discoverivan.gameshop.db.JDBCTemplate;
import ru.discoverivan.gameshop.db.entity.Game;
import ru.discoverivan.gameshop.db.entity.Order;

import java.util.Date;
import java.util.List;

@Repository
public class H2OrderDAO implements OrderDAO {

    private H2EntityDAO<Order> entityDAO;

    @Autowired
    public void setEntityDAO(H2EntityDAO<Order> entityDAO) {
        this.entityDAO = entityDAO;
    }

    @Override
    public void add(Order order) {
        entityDAO.add(order);
    }

    @Override
    public Order get(int id) {
        Session session= HibernateUtil.getSession();
        session.beginTransaction();
        Query query = session.createQuery("from Order where id=:id");
        query.setParameter("id", id);
        Order order= (Order) query.uniqueResult();
        session.getTransaction().commit();
        session.close();
        return order;
    }

    @Override
    public List<Order> getAll() {
        Session session= HibernateUtil.getSession();
        session.beginTransaction();
        List<Order> orders = (List<Order>) session.createQuery("from Order").list();
        session.getTransaction().commit();
        session.close();
        return orders;
    }

    public List<Order> getOrdersInTimeInterval(Date from, Date to){
        Session session= HibernateUtil.getSession();
        session.beginTransaction();
        Query query = session.createQuery(
                "from Order where (date >= :beginDate and date <= :endDate)"
        );
        query.setParameter("beginDate", from);
        query.setParameter("endDate", to);
        List<Order> orders = (List<Order>) query.list();
        session.getTransaction().commit();
        session.close();
        return orders;
    }

    @Override
    public void delete(int id) {
        Session session=HibernateUtil.getSession();
        session.beginTransaction();
        Query query=session.createQuery("from Order where id=:id");
        query.setParameter("id",id);
        Order order= (Order) query.uniqueResult();
        session.delete(order);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void update(Order updatedOrder) {
        entityDAO.update(updatedOrder);
    }

    @Override
    public int count() {
        Session session= HibernateUtil.getSession();
        session.beginTransaction();
        Long count = (Long) session.createQuery("select count(*) from Order ").uniqueResult();
        session.getTransaction().commit();
        session.close();
        return count.intValue();
    }

    @Override
    public void clear() {
        Session session=HibernateUtil.getSession();
        session.beginTransaction();
        session.createQuery("delete from Order").executeUpdate();
        session.getTransaction().commit();
        session.close();
    }
}
