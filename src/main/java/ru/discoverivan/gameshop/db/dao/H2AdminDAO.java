package ru.discoverivan.gameshop.db.dao;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.discoverivan.gameshop.db.HibernateUtil;
import ru.discoverivan.gameshop.db.entity.Admin;
import ru.discoverivan.gameshop.db.entity.User;

import java.util.List;

@Repository
public class H2AdminDAO implements AdminDAO {

    private H2EntityDAO<Admin> entityDAO;

    @Autowired
    public void setEntityDAO(H2EntityDAO<Admin> entityDAO) {
        this.entityDAO = entityDAO;
    }

    @Override
    public void add(Admin admin) {
        entityDAO.add(admin);
    }

    @Override
    public Admin get(int id) {
        Session session= HibernateUtil.getSession();
        session.beginTransaction();
        Query query = session.createQuery("from Admin where id=:id");
        query.setParameter("id", id);
        Admin admin= (Admin) query.uniqueResult();
        session.getTransaction().commit();
        session.close();
        return admin;
    }

    @Override
    public Admin findByUsername(String username) {
        Session session= HibernateUtil.getSession();
        session.beginTransaction();
        Query query = session.createQuery("from Admin where username=:username");
        query.setParameter("username", username);
        Admin admin = (Admin) query.uniqueResult();
        session.getTransaction().commit();
        session.close();
        return admin;
    }

    @Override
    public List<Admin> getAll() {
        Session session= HibernateUtil.getSession();
        session.beginTransaction();
        List<Admin> admins = (List<Admin>) session.createQuery("from Admin").list();
        session.getTransaction().commit();
        session.close();
        return admins;
    }

    @Override
    public void delete(int id) {
        Session session=HibernateUtil.getSession();
        session.beginTransaction();
        Query query=session.createQuery("from Admin where id=:id");
        query.setParameter("id",id);
        Admin admin= (Admin) query.uniqueResult();
        session.delete(admin);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void update(Admin updatedAdmin) {
        entityDAO.update(updatedAdmin);
    }

    @Override
    public int count() {
        Session session= HibernateUtil.getSession();
        session.beginTransaction();
        Long count = (Long) session.createQuery("select count(*) from Admin ").uniqueResult();
        session.getTransaction().commit();
        session.close();
        return count.intValue();
    }

    @Override
    public void clear() {
        Session session=HibernateUtil.getSession();
        session.beginTransaction();
        session.createQuery("delete from Admin ").executeUpdate();
        session.getTransaction().commit();
        session.close();
    }
}
