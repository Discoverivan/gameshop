package ru.discoverivan.gameshop.db.dao;

import ru.discoverivan.gameshop.db.entity.Genre;
import ru.discoverivan.gameshop.db.entity.Language;

import java.util.List;

public interface GenreDAO {
    void add(Genre genre);

    Genre get(int id);

    List<Genre> getAll();

    void delete(int id);

    void update(Genre updatedGenre);

    void clear();
}
