package ru.discoverivan.gameshop.db.dao;

import ru.discoverivan.gameshop.db.entity.Game;
import ru.discoverivan.gameshop.db.entity.Product;

import java.util.List;

public interface ProductDAO {
    void add(Product product);

    Product get(int id);

    List<Product> getAllProductsByGame(Game game);

    List<Product> getAll();

    void delete(int id);

    void update(Product updatedProduct);

    int count();

    void clear();
}
