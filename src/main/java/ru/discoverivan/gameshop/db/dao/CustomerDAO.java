package ru.discoverivan.gameshop.db.dao;

import ru.discoverivan.gameshop.db.entity.Customer;
import ru.discoverivan.gameshop.db.entity.Order;
import ru.discoverivan.gameshop.db.entity.User;

import java.util.List;

public interface CustomerDAO {
    void add(Customer customer);

    Customer get(int id);

    Customer findByEmail(String email);

    List<Customer> getAll();

    void delete(int id);

    void update(Customer updatedCustomer);

    int count();

    void clear();

    Customer findByUsername(String username);
}
