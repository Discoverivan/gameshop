package ru.discoverivan.gameshop.db.dao;

import ru.discoverivan.gameshop.db.entity.additional.Cart;

import java.util.List;

public interface CartDAO {
    void add(Cart cart);

    Cart get(int id);

    List<Cart> getAll();

    void delete(int id);

    void update(Cart updatedCart);

    void clear();
}
