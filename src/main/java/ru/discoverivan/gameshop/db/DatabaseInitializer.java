package ru.discoverivan.gameshop.db;

import java.util.ArrayList;
import java.util.List;

public class DatabaseInitializer {
    private List<String> requiredTables = new ArrayList<>();
    private static final String sqlScheme =
            "CREATE TABLE `game` (\n" +
                    "\t`id` INT NOT NULL AUTO_INCREMENT,\n" +
                    "\t`title` varchar NOT NULL,\n" +
                    "\t`description` TEXT NOT NULL,\n" +
                    "\t`release_date` DATE,\n" +
                    "\t`image_url` varchar NOT NULL,\n" +
                    "\t`price` FLOAT NOT NULL DEFAULT '0',\n" +
                    "\t`amount` INT NOT NULL DEFAULT '0',\n" +
                    "\t`views` INT NOT NULL DEFAULT '0',\n" +
                    "\t`status` INT NOT NULL,\n" +
                    "\t`publish_date` DATE NOT NULL,\n" +
                    "\tPRIMARY KEY (`id`)\n" +
                    ");\n" +
                    "\n" +
                    "CREATE TABLE `order` (\n" +
                    "\t`id` INT NOT NULL AUTO_INCREMENT,\n" +
                    "\t`date` DATE NOT NULL,\n" +
                    "\t`customer_id` INT NOT NULL,\n" +
                    "\t`game_id` INT NOT NULL,\n" +
                    "\t`game_key` varchar NOT NULL,\n" +
                    "\tPRIMARY KEY (`id`)\n" +
                    ");\n" +
                    "\n" +
                    "CREATE TABLE `customer` (\n" +
                    "\t`id` INT NOT NULL AUTO_INCREMENT,\n" +
                    "\t`name` varchar NOT NULL,\n" +
                    "\t`email` varchar NOT NULL,\n" +
                    "\tPRIMARY KEY (`id`)\n" +
                    ");\n" +
                    "\n" +
                    "CREATE TABLE `game_language` (\n" +
                    "\t`id` INT NOT NULL AUTO_INCREMENT,\n" +
                    "\t`game_id` INT NOT NULL,\n" +
                    "\t`language_id` INT NOT NULL,\n" +
                    "\tPRIMARY KEY (`id`)\n" +
                    ");\n" +
                    "\n" +
                    "CREATE TABLE `language` (\n" +
                    "\t`id` INT NOT NULL AUTO_INCREMENT,\n" +
                    "\t`name` varchar NOT NULL,\n" +
                    "\tPRIMARY KEY (`id`)\n" +
                    ");\n" +
                    "\n" +
                    "CREATE TABLE `platform` (\n" +
                    "\t`id` INT NOT NULL AUTO_INCREMENT,\n" +
                    "\t`name` varchar NOT NULL,\n" +
                    "\tPRIMARY KEY (`id`)\n" +
                    ");\n" +
                    "\n" +
                    "CREATE TABLE `genre` (\n" +
                    "\t`id` INT NOT NULL AUTO_INCREMENT,\n" +
                    "\t`name` varchar NOT NULL,\n" +
                    "\tPRIMARY KEY (`id`)\n" +
                    ");\n" +
                    "\n" +
                    "CREATE TABLE `game_genre` (\n" +
                    "\t`id` INT NOT NULL AUTO_INCREMENT,\n" +
                    "\t`game_id` INT NOT NULL,\n" +
                    "\t`genre_id` INT NOT NULL,\n" +
                    "\tPRIMARY KEY (`id`)\n" +
                    ");\n" +
                    "\n" +
                    "CREATE TABLE `game_platform` (\n" +
                    "\t`id` INT NOT NULL AUTO_INCREMENT,\n" +
                    "\t`game_id` INT NOT NULL,\n" +
                    "\t`platform_id` INT NOT NULL,\n" +
                    "\tPRIMARY KEY (`id`)\n" +
                    ");\n" +
                    "\n" +
                    "CREATE TABLE `game_screenshot` (\n" +
                    "\t`id` INT NOT NULL AUTO_INCREMENT,\n" +
                    "\t`game_id` INT NOT NULL,\n" +
                    "\t`image_url` varchar NOT NULL,\n" +
                    "\tPRIMARY KEY (`id`)\n" +
                    ");\n" +
                    "\n" +
                    "ALTER TABLE `order` ADD CONSTRAINT `order_fk0` FOREIGN KEY (`customer_id`) REFERENCES `customer`(`id`);\n" +
                    "\n" +
                    "ALTER TABLE `order` ADD CONSTRAINT `order_fk1` FOREIGN KEY (`game_id`) REFERENCES `game`(`id`);\n" +
                    "\n" +
                    "ALTER TABLE `game_language` ADD CONSTRAINT `game_language_fk0` FOREIGN KEY (`game_id`) REFERENCES `game`(`id`);\n" +
                    "\n" +
                    "ALTER TABLE `game_language` ADD CONSTRAINT `game_language_fk1` FOREIGN KEY (`language_id`) REFERENCES `language`(`id`);\n" +
                    "\n" +
                    "ALTER TABLE `game_genre` ADD CONSTRAINT `game_genre_fk0` FOREIGN KEY (`game_id`) REFERENCES `game`(`id`);\n" +
                    "\n" +
                    "ALTER TABLE `game_genre` ADD CONSTRAINT `game_genre_fk1` FOREIGN KEY (`genre_id`) REFERENCES `genre`(`id`);\n" +
                    "\n" +
                    "ALTER TABLE `game_platform` ADD CONSTRAINT `game_platform_fk0` FOREIGN KEY (`game_id`) REFERENCES `game`(`id`);\n" +
                    "\n" +
                    "ALTER TABLE `game_platform` ADD CONSTRAINT `game_platform_fk1` FOREIGN KEY (`platform_id`) REFERENCES `platform`(`id`);\n" +
                    "\n" +
                    "ALTER TABLE `game_screenshot` ADD CONSTRAINT `game_screenshot_fk0` FOREIGN KEY (`game_id`) REFERENCES `game`(`id`);\n";

    public DatabaseInitializer() {
        requiredTables.add("games");
        requiredTables.add("orders");
    }

    public void Initialize(){

    }
}
