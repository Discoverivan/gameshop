package ru.discoverivan.gameshop.db.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "game")
public class Game implements Serializable {
    private int id;
    private String title;
    private String description;
    private String posterImageUrl;
    private Set<String> screenshotsUrls;
    private Set<Language> languages;
    private Set<Platform> platforms;
    private Set<Genre> genres;
    private Date releaseDate;
    private String developer;
    private float price;
    private int amount;
    private int views;
    private int orderCount;
    private int status;
    private Date publishDate;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(name = "description", columnDefinition = "TEXT")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "poster_image_url")
    public String getPosterImageUrl() {
        return posterImageUrl;
    }

    public void setPosterImageUrl(String posterImageUrl) {
        this.posterImageUrl = posterImageUrl;
    }

    @ElementCollection(fetch=FetchType.EAGER)
    @CollectionTable(name = "game_screenshot", joinColumns = @JoinColumn(name = "game_id"))
    @Column(name = "image_url")
    public Set<String> getScreenshotsUrls() {
        return screenshotsUrls;
    }

    public void setScreenshotsUrls(Set<String> screenshotsUrls) {
        this.screenshotsUrls = screenshotsUrls;
    }

    @ManyToMany(fetch=FetchType.EAGER)
    @JoinTable(name = "game_language", joinColumns = {
            @JoinColumn(name = "game_id", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "language_id",
                    nullable = false, updatable = false) })
    public Set<Language> getLanguages() {
        Arrays.sort(languages.toArray());
        return languages;
    }

    public void setLanguages(Set<Language> languages) {
        this.languages = languages;
    }

    @ManyToMany(fetch=FetchType.EAGER)
    @JoinTable(name = "game_platform", joinColumns = {
            @JoinColumn(name = "game_id", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "platform_id",
                    nullable = false, updatable = false) })
    public Set<Platform> getPlatforms() {
        return platforms;
    }

    public void setPlatforms(Set<Platform> platforms) {
        this.platforms = platforms;
    }

    @ManyToMany(fetch=FetchType.EAGER)
    @JoinTable(name = "game_genre", joinColumns = {
            @JoinColumn(name = "game_id", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "genre_id",
                    nullable = false, updatable = false) })
    public Set<Genre> getGenres() {
        return genres;
    }

    public void setGenres(Set<Genre> genres) {
        this.genres = genres;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "release_date")
    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    @Column(name = "developer")
    public String getDeveloper() {
        return developer;
    }

    public void setDeveloper(String developer) {
        this.developer = developer;
    }

    @Column(name = "price")
    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    @Column(name = "amount")
    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Column(name = "views")
    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    @Column(name = "order_count")
    public int getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(int orderCount) {
        this.orderCount = orderCount;
    }

    @Column(name = "status")
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "publish_date", nullable = false, columnDefinition="DATETIME")
    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }
}
