package ru.discoverivan.gameshop.db.entity.additional;

public class FullStats {
    private Integer ordersCount;
    private Float ordersSum;
    private GraphData ordersGraph;

    public Integer getOrdersCount() {
        return ordersCount;
    }

    public void setOrdersCount(Integer ordersCount) {
        this.ordersCount = ordersCount;
    }

    public Float getOrdersSum() {
        return ordersSum;
    }

    public void setOrdersSum(Float ordersSum) {
        this.ordersSum = ordersSum;
    }

    public GraphData getOrdersGraph() {
        return ordersGraph;
    }

    public void setOrdersGraph(GraphData ordersGraph) {
        this.ordersGraph = ordersGraph;
    }
}
