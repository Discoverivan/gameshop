package ru.discoverivan.gameshop.db.entity;

public enum UserRolesEnum {
    ADMIN,
    CUSTOMER;
}
