package ru.discoverivan.gameshop.db.entity.additional;

import ru.discoverivan.gameshop.db.entity.Customer;
import ru.discoverivan.gameshop.db.entity.Game;

import java.util.Set;

public class Cart {
    private int id;
    private Customer customer;
    private Set<Game> games;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Set<Game> getGames() {
        return games;
    }

    public void setGames(Set<Game> games) {
        this.games = games;
    }
}
