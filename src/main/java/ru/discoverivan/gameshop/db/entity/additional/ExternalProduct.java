package ru.discoverivan.gameshop.db.entity.additional;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ExternalProduct {
    @JsonProperty("game_id")
    private String gameId;

    @JsonProperty("licence_key")
    private String licenceKey;

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getLicenceKey() {
        return licenceKey;
    }

    public void setLicenceKey(String licenceKey) {
        this.licenceKey = licenceKey;
    }
}
