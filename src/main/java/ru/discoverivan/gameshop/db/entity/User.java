package ru.discoverivan.gameshop.db.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "user")
@DiscriminatorColumn(name = "discriminator")
public class User {
    private int id;
    private String username;
    private String password;
    private Set<UserRolesEnum> roles;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "username", unique = true)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"))
    @Enumerated(EnumType.STRING)
    @Column(name = "role")
    public Set<UserRolesEnum> getRoles() {
        return roles;
    }

    public void setRoles(Set<UserRolesEnum> roles) {
        this.roles = roles;
    }
}
