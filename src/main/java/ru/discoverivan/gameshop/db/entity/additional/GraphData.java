package ru.discoverivan.gameshop.db.entity.additional;

import java.util.*;

public class GraphData {
    private List<String> labels;
    private List<Number> data;

    public GraphData(Map<String, Integer> m) {
        labels = new ArrayList<>();
        data = new ArrayList<>();
        List<String> keys = new ArrayList<>(m.keySet());
        Collections.sort(keys);
        for (String s : keys) {
            labels.add(s);
            data.add(m.get(s));
        }
    }

    public List<String> getLabels() {
        return labels;
    }

    public void setLabels(List<String> labels) {
        this.labels = labels;
    }

    public List<Number> getData() {
        return data;
    }

    public void setData(List<Number> data) {
        this.data = data;
    }
}
