package ru.discoverivan.gameshop.tags;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.discoverivan.gameshop.domain.CartService;

import javax.servlet.http.Cookie;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.util.Set;

public class GetCartNumTag extends SimpleTagSupport {

    private String cartCookie;

    private CartService cartService;

    public void setCartCookie(String cartCookie) {
        this.cartCookie = cartCookie;
    }

    @Override
    public void doTag() throws JspException, IOException {
        cartService = new CartService();
        Set<Integer> cart = cartService.getCartFromCookie(cartCookie);
        getJspContext().getOut().write(Integer.toString(cart.size()));
    }
}
