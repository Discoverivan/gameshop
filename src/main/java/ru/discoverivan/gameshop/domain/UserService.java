package ru.discoverivan.gameshop.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ru.discoverivan.gameshop.db.dao.CustomerDAO;
import ru.discoverivan.gameshop.db.entity.Customer;
import ru.discoverivan.gameshop.db.entity.UserRolesEnum;

import java.util.HashSet;
import java.util.Set;

@Service
public class UserService{

    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private CustomerDAO customerDAO;

    @Autowired
    public void setCustomerDAO(CustomerDAO customerDAO) {
        this.customerDAO = customerDAO;
    }

    @Autowired
    public void setbCryptPasswordEncoder(BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public void registerNewCustomer(Customer customer){
        customer.setPassword(bCryptPasswordEncoder.encode(customer.getPassword()));
        Set<UserRolesEnum> roles = new HashSet<>();
        roles.add(UserRolesEnum.CUSTOMER);
        customer.setRoles(roles);
        customerDAO.add(customer);
    }

    public Customer findByUsername(String username) {
        return customerDAO.findByUsername(username);
    }
}
