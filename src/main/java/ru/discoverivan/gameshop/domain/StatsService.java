package ru.discoverivan.gameshop.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.discoverivan.gameshop.db.dao.*;
import ru.discoverivan.gameshop.db.entity.Order;
import ru.discoverivan.gameshop.db.entity.Product;
import ru.discoverivan.gameshop.db.entity.additional.FullStats;
import ru.discoverivan.gameshop.db.entity.additional.GraphData;
import ru.discoverivan.gameshop.db.entity.additional.ShortStats;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class StatsService {

    private GameDAO gameDAO;
    private OrderDAO orderDAO;
    private CustomerDAO customerDAO;

    @Autowired
    public void setGameDAO(GameDAO gameDAO) {
        this.gameDAO = gameDAO;
    }

    @Autowired
    public void setOrderDAO(OrderDAO orderDAO) {
        this.orderDAO = orderDAO;
    }

    @Autowired
    public void setCustomerDAO(CustomerDAO customerDAO) {
        this.customerDAO = customerDAO;
    }

    private int getTotalGameCount(){
        return gameDAO.count();
    }

    private int getTotalOrderCount(){
        return orderDAO.count();
    }

    private int getTotalCustomerCount(){
        return customerDAO.count();
    }

    public ShortStats getShortStats(){
        ShortStats shortStats = new ShortStats();
        shortStats.setTotalOrders(getTotalOrderCount());
        shortStats.setTotalGames(getTotalGameCount());
        shortStats.setTotalCustomers(getTotalCustomerCount());
        return shortStats;
    }

    private float getOrderSum(List<Order> orders){
        float sum = 0;
        for (Order order : orders) {
            List<Product> products = new ArrayList<>(order.getProducts());
            for (Product product : products) {
                sum += product.getGame().getPrice();
            }
        }
        return sum;
    }

    public FullStats getFullStats(String timeInterval) {
        Date startDate = getStartDate(timeInterval);
        List<Order> orders;
        if (!timeInterval.equals("all")){
            orders = orderDAO.getOrdersInTimeInterval(startDate, new Date());
        } else {
            orders = orderDAO.getAll();
        }

        FullStats fullStats = new FullStats();
        fullStats.setOrdersSum(getOrderSum(orders));
        fullStats.setOrdersCount(orders.size());
        fullStats.setOrdersGraph(getOrdersGraphData(orders));
        return fullStats;
    }

    private GraphData getOrdersGraphData(List<Order> orders) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Map<String, Integer> data = new HashMap<>();
        for (Order order : orders) {
            if (!data.containsKey(format.format(order.getDate()))) {
                data.put(format.format(order.getDate()), 1);
            } else {
                data.put(format.format(order.getDate()), data.get(format.format(order.getDate()))+1);
            }
        }
        return new GraphData(data);
    }

    private Date getStartDate(String timeInterval) {
        Date startDate = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(startDate);
        switch (timeInterval){
            case "today":
                cal.add(Calendar.DATE, -1);
                break;
            case "7":
                cal.add(Calendar.DATE, -7);
                break;
            case "30":
                cal.add(Calendar.DATE, -30);
                break;
            case "90":
                cal.add(Calendar.DATE, -90);
                break;
            case "180":
                cal.add(Calendar.DATE, -180);
                break;
        }
        if (timeInterval != "all"){
            startDate = cal.getTime();
        }
        return startDate;
    }
}
