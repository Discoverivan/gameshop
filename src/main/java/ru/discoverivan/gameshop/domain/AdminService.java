package ru.discoverivan.gameshop.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ru.discoverivan.gameshop.db.dao.*;
import ru.discoverivan.gameshop.db.entity.*;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Service
public class AdminService {

    private GameDAO gameDAO;
    private OrderDAO orderDAO;
    private CustomerDAO customerDAO;
    private LanguageDAO languageDAO;
    private GenreDAO genreDAO;
    private PlatformDAO platformDAO;
    private AdminDAO adminDAO;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public void addGame(Game game) {
        game.setPublishDate(new Date());
        gameDAO.add(game);
    }

    @Autowired
    public void setbCryptPasswordEncoder(BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Autowired
    public void setAdminDAO(AdminDAO adminDAO) {
        this.adminDAO = adminDAO;
    }

    @Autowired
    public void setGameDAO(GameDAO gameDAO) {
        this.gameDAO = gameDAO;
    }

    @Autowired
    public void setOrderDAO(OrderDAO orderDAO) {
        this.orderDAO = orderDAO;
    }

    @Autowired
    public void setCustomerDAO(CustomerDAO customerDAO) {
        this.customerDAO = customerDAO;
    }

    @Autowired
    public void setLanguageDAO(LanguageDAO languageDAO) {
        this.languageDAO = languageDAO;
    }

    @Autowired
    public void setGenreDAO(GenreDAO genreDAO) {
        this.genreDAO = genreDAO;
    }

    @Autowired
    public void setPlatformDAO(PlatformDAO platformDAO) {
        this.platformDAO = platformDAO;
    }

    public void addLanguage(Language language){
        languageDAO.add(language);
    }

    public void addGenre(Genre genre){
        genreDAO.add(genre);
    }

    public void addPlatform(Platform platform){
        platformDAO.add(platform);
    }

    public void editGame(Integer id, Game game) {
        Game oldGame = gameDAO.get(id);
        game.setId(oldGame.getId());
        game.setPublishDate(oldGame.getPublishDate());
        gameDAO.update(game);
    }

    public void deleteGame(Integer id){
        gameDAO.delete(id);
    }

    public void deleteOrder(int id) {
        orderDAO.delete(id);
    }

    public void deleteLanguage(int id) {languageDAO.delete(id);}

    public void deleteGenre(int id) {genreDAO.delete(id);}

    public void deletePlatform(int id) {platformDAO.delete(id);}

    public void editLanguage(Integer id, Language language) {
        Language editableLanguage = languageDAO.get(id);

        editableLanguage.setName(language.getName());
        languageDAO.update(editableLanguage);
    }

    public void editGenre(Integer id, Genre genre) {
        Genre editabelGenre = genreDAO.get(id);

        editabelGenre.setName(genre.getName());
        genreDAO.update(editabelGenre);
    }

    public void editPlatform(Integer id, Platform platform) {
        Platform editablePlatform = platformDAO.get(id);

        editablePlatform.setName(platform.getName());
        platformDAO.update(editablePlatform);
    }

    public void deleteCustomer(int id) {
        customerDAO.delete(id);
    }

    public void registerNewAdmin(Admin admin){
        admin.setPassword(bCryptPasswordEncoder.encode(admin.getPassword()));
        Set<UserRolesEnum> roles = new HashSet<>();
        roles.add(UserRolesEnum.ADMIN);
        admin.setRoles(roles);
        adminDAO.add(admin);
    }

    public void removeAdmin(int id){
        adminDAO.delete(id);
    }
}
