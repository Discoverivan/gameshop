package ru.discoverivan.gameshop.domain;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PaginationService<T> {
    public List<List<T>> getPages(List<T> objects, int objectsPerPage){
        List<List<T>> pages = new ArrayList<>();
        int pagesCount = (int) Math.ceil((double) objects.size() / objectsPerPage);
        int objectPointer = 0;
        for (int i=0;i<pagesCount;i++){
            pages.add(new ArrayList<>());
            for(int j=0;j<objectsPerPage;j++){
                if (objectPointer >= objects.size())
                    break;
                pages.get(i).add(objects.get(objectPointer++));
            }
        }
        return pages;
    }
}
