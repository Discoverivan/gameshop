package ru.discoverivan.gameshop.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.discoverivan.gameshop.db.dao.GameDAO;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

@Service
public class CartService {
    private GameDAO gameDAO;

    public String addGameToCartCookie(String cartCookie, int gameId){
        Set<Integer> cart = getCartFromCookie(cartCookie);
        cart.add(gameId);
        return getCookieFromCart(cart);
    }

    public String removeGameFromCartCookie(String cartCookie, Integer gameId) {
        Set<Integer> cart = getCartFromCookie(cartCookie);
        cart.remove(gameId);
        return getCookieFromCart(cart);
    }

    public Set<Integer> getCartFromCookie(String cartCookie) {
        ObjectMapper mapper = new ObjectMapper();
        if (cartCookie == null){
            cartCookie = "";
        } else {
            try {
                cartCookie = URLDecoder.decode(cartCookie, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                cartCookie = "";
            }
        }
        try {
            return mapper.readValue(cartCookie, mapper.getTypeFactory().constructCollectionType(HashSet.class, Integer.class));
        } catch (IOException e) {
            return new HashSet<>();
        }
    }

    public String getCookieFromCart(Set<Integer> cart) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return URLEncoder.encode(mapper.writeValueAsString(cart), "UTF-8");
        } catch (IOException e) {
            return "";
        }
    }

    public Set<Integer> validateCart(Set<Integer> cart) {
        Set<Integer> newCart = new HashSet<>();
        for (Integer gid : cart) {
            if (gameDAO.get(gid) != null){
                newCart.add(gid);
            }
        }
        return newCart;
    }

    @Autowired
    public void setGameDAO(GameDAO gameDAO) {
        this.gameDAO = gameDAO;
    }
}
