package ru.discoverivan.gameshop.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.discoverivan.gameshop.db.dao.CustomerDAO;
import ru.discoverivan.gameshop.db.dao.GameDAO;
import ru.discoverivan.gameshop.db.dao.OrderDAO;
import ru.discoverivan.gameshop.db.dao.ProductDAO;
import ru.discoverivan.gameshop.db.entity.*;

import javax.mail.MessagingException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Pattern;

@Service
public class ShopService {

    private EmailService emailService;
    private GameDAO gameDAO;
    private OrderDAO orderDAO;
    private CustomerDAO customerDAO;
    private ProductDAO productDAO;

    @Autowired
    public void setProductDAO(ProductDAO productDAO) {
        this.productDAO = productDAO;
    }

    @Autowired
    public void setEmailService(EmailService emailService) {
        this.emailService = emailService;
    }

    @Autowired
    public void setGameDAO(GameDAO gameDAO) {
        this.gameDAO = gameDAO;
    }

    @Autowired
    public void setOrderDAO(OrderDAO orderDAO) {
        this.orderDAO = orderDAO;
    }

    @Autowired
    public void setCustomerDAO(CustomerDAO customerDAO) {
        this.customerDAO = customerDAO;
    }

    public void gameViewed(int gameId){
        Game game = gameDAO.get(gameId);
        if(game != null) {
            game.setViews(game.getViews() + 1);
            gameDAO.update(game);
        }
    }

    public Order createOrder(Set<Integer> gameIds, Customer customer){
        Order newOrder = new Order();
        newOrder.setCustomer(customer);
        newOrder.setDate(new Date());
        Set<Game> games = new HashSet<>();
        for (Integer gameId : gameIds) {
            games.add(gameDAO.get(gameId));
        }
        Set<Product> products = new HashSet<>();
        for (Game game : games) {
            Product product = new Product();
            product.setGame(game);
            product.setKey(generateLicenseKey());
            productDAO.add(product);
            products.add(product);
        }
        newOrder.setProducts(products);
        orderDAO.add(newOrder);
        return newOrder;
    }

    public void processOrder(Order order){
        String subjectText = String.format("Заказ №%s в магазине Gameshop", order.getId());
        StringBuffer emailText = new StringBuffer();
        emailText.append(
                String.format("<h1>Спасибо за покупку!</h1>" +
                        "<h3>Здравствуйте, %s!</h3>"+
                        "<h3>Детали заказа №%s:</h3>",
                order.getCustomer().getName(),
                order.getId()));

        System.out.println(order.getProducts().size());
        for (Product product : order.getProducts()) {
            emailText.append(
                    String.format("<p>Ключ для игры <b>%s</b>: %s</p>",
                    product.getGame().getTitle(),
                    product.getKey()));
        }

        try {
            emailService.sendEmail(order.getCustomer().getEmail(),subjectText, emailText.toString());
        } catch (MessagingException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private List<Game> getOrderGames(Order order){
        return null;
    }

    private String licenseKeySymbols = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    private String generateLicenseKey(){
        StringBuffer s = new StringBuffer();
        Random r = new Random();

        for (int i = 0; i < 4;i++){
            for (int j = 0; j < 5; j++){
                s.append(licenseKeySymbols.charAt(r.nextInt(licenseKeySymbols.length())));
            }
            s.append('-');
        }
        s.deleteCharAt(s.length()-1);
        return s.toString();
    }
}
