package ru.discoverivan.gameshop.domain;

import com.sun.mail.smtp.SMTPTransport;
import org.springframework.stereotype.Service;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.websocket.server.ServerEndpoint;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;

@Service
public class EmailService {

    public void sendEmail(String email, String subject, String text) throws MessagingException, UnsupportedEncodingException {
        Session session = getSession();
        Message msg = getMessage(email, subject, text, session);
        transportMessageViaSmtp(session, msg);
    }

    private void transportMessageViaSmtp(Session session, Message msg) throws MessagingException {
        SMTPTransport t = (SMTPTransport)session.getTransport("smtps");
        t.connect(System.getProperty("mail.smtps.host"), "gameshop@discoverivan.ru", "root123456");
        t.sendMessage(msg, msg.getAllRecipients());
        t.close();
    }

    private Message getMessage(String toEmail, String subject, String text, Session session) throws MessagingException, UnsupportedEncodingException {
        Message msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress("gameshop@discoverivan.ru", "GAMESHOP - Магазин игр"));
        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));
        msg.setSubject(subject);
        msg.setContent(text,"text/html; charset=utf-8");
        msg.setHeader("X-Mailer", "ShopService");
        msg.setSentDate(new Date());
        return msg;
    }

    private Session getSession() {
        Properties props = System.getProperties();
        props.put("mail.smtps.host","smtp.yandex.com");
        props.put("mail.smtps.auth","true");
        return Session.getInstance(props, null);
    }
}
