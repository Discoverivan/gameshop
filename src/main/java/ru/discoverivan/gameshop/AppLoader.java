package ru.discoverivan.gameshop;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import ru.discoverivan.gameshop.db.HibernateUtil;
import ru.discoverivan.gameshop.db.dao.*;
import ru.discoverivan.gameshop.db.entity.Game;
import ru.discoverivan.gameshop.db.entity.Genre;
import ru.discoverivan.gameshop.db.entity.Language;
import ru.discoverivan.gameshop.db.entity.Platform;
import ru.discoverivan.gameshop.domain.AdminService;

import javax.servlet.ServletContextListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class AppLoader implements ServletContextListener {

    private LanguageDAO languageDAO;
    private PlatformDAO platformDAO;
    private GenreDAO genreDAO;
    private GameDAO gameDAO;
    private OrderDAO orderDAO;
    private AdminService adminService;

    @Autowired
    public void setOrderDAO(OrderDAO orderDAO) {
        this.orderDAO = orderDAO;
    }

    @Autowired
    public void setLanguageDAO(LanguageDAO languageDAO) {
        this.languageDAO = languageDAO;
    }

    @Autowired
    public void setPlatformDAO(PlatformDAO platformDAO) {
        this.platformDAO = platformDAO;
    }

    @Autowired
    public void setGenreDAO(GenreDAO genreDAO) {
        this.genreDAO = genreDAO;
    }

    @Autowired
    public void setGameDAO(GameDAO gameDAO) {
        this.gameDAO = gameDAO;
    }

    @Autowired
    public void setAdminService(AdminService adminService) {
        this.adminService = adminService;
    }

    public void loadTest(int count){
        orderDAO.clear();
        gameDAO.clear();
        genreDAO.clear();
        platformDAO.clear();
        languageDAO.clear();

        Genre genre = new Genre();
        genre.setName("Action");
        adminService.addGenre(genre);

        Language language = new Language();
        language.setName("Русский");
        adminService.addLanguage(language);

        Platform platform = new Platform();
        platform.setName("PC");
        adminService.addPlatform(platform);

        Game game = new Game();
        for(int i=0;i<count;i++){
            game.setTitle("Game "+i);
            game.setDescription("Description");
            game.setPlatforms(new HashSet<>(platformDAO.getAll()));
            game.setLanguages(new HashSet<>(languageDAO.getAll()));
            game.setGenres(new HashSet<>(genreDAO.getAll()));
            game.setDeveloper("Developer");
            game.setReleaseDate(new Date());
            game.setPosterImageUrl("https://via.placeholder.com/400x400");
            Set<String> screens = new HashSet<>();
            screens.add("https://via.placeholder.com/400x400");
            game.setScreenshotsUrls(screens);
            game.setPrice(i);
            adminService.addGame(game);
        }
    }

    public void initBase() {
        orderDAO.clear();
        gameDAO.clear();
        genreDAO.clear();
        platformDAO.clear();
        languageDAO.clear();

        Genre genre = new Genre();
        genre.setName("Action");
        adminService.addGenre(genre);

        Language language = new Language();
        language.setName("Русский");
        adminService.addLanguage(language);

        Platform platform = new Platform();
        platform.setName("PC");
        adminService.addPlatform(platform);

        Game game = new Game();
        game.setTitle("Far Cry 5");
        game.setDescription("В этот раз действие знаменитой серии Far Cry происходит в Америке.\n" +
                "Добро пожаловать в Монтану - округ Хоуп - земли свободных и смелых, захваченные фанатиками Врат Эдема. Дайте отпор Иосифу Сиду и его сторонникам. Разожгите огонь сопротивления.\n" +
                "Отправляйтесь в США, сокрушите сектантов в одиночку или с другом, но бойтесь гнева Иосифа Сида и его последователей. Создайте уникального персонажа! Используйте в борьбе с сектантами разнообразную технику.");
        game.setPlatforms(new HashSet<>(platformDAO.getAll()));
        game.setLanguages(new HashSet<>(languageDAO.getAll()));
        game.setGenres(new HashSet<>(genreDAO.getAll()));
        game.setDeveloper("Ubisoft");
        try {
            game.setReleaseDate(new SimpleDateFormat("yyyy-MM-dd").parse("2018-03-27"));
        } catch (ParseException e) {
            game.setReleaseDate(new Date());
        }
        game.setPosterImageUrl("https://steamcdn-a.akamaihd.net/steam/apps/552520/header.jpg?t=1527102722");
        Set<String> screens = new HashSet<>();
        screens.add("https://steamcdn-a.akamaihd.net/steam/apps/552520/ss_54e115519104798a3a2dc55e6de3d4974e144b77.600x338.jpg?t=1527102722");
        screens.add("https://steamcdn-a.akamaihd.net/steam/apps/552520/ss_c6f08b3d2e156f705205f882504d3cd96f78cca1.600x338.jpg?t=1527102722");
        game.setScreenshotsUrls(screens);
        game.setPrice(1999);
        adminService.addGame(game);

        game = new Game();
        game.setTitle("Rise of the Tomb Raider");
        game.setDescription("Издание Rise of the Tomb Raider: 20 Year Celebration включает в себя базовую версию игры и сезонный пропуск со всеми доступными дополнениями. Исследуйте поместье Крофт в новом дополнении Blood Ties и защищайте его от зомби в режиме Lara's Nightmare. Боритесь за выживание вместе с друзьями в новом кооперативном онлайн-режиме Endurance и проверьте себя на прочность с новой сложностью «Экстремальное выживание». В пакет дополнительного контента входят костюм и оружие для Лары по мотивам Tomb Raider 3, а также 5 других классических костюмов. Два дополнения доступны для скачивания уже сейчас: в Baba Yaga: The Temple of the Witch вам предстоит исследовать новую загадочную гробницу, полную древних ужасов, а в Cold Darkness Awakened — отбиваться от полчищ зараженных монстров.");
        game.setPlatforms(new HashSet<>(platformDAO.getAll()));
        game.setLanguages(new HashSet<>(languageDAO.getAll()));
        game.setGenres(new HashSet<>(genreDAO.getAll()));
        game.setDeveloper("Square Enix");
        try {
            game.setReleaseDate(new SimpleDateFormat("yyyy-MM-dd").parse("2016-02-09"));
        } catch (ParseException e) {
            game.setReleaseDate(new Date());
        }
        game.setPosterImageUrl("https://steamcdn-a.akamaihd.net/steam/apps/391220/header.jpg?t=1532513501");
        screens = new HashSet<>();
        screens.add("https://steamcdn-a.akamaihd.net/steam/apps/391220/ss_4ef0868ecfc6b19bc1af18b88cabe33fe8147cf7.600x338.jpg?t=1532513501");
        screens.add("https://steamcdn-a.akamaihd.net/steam/apps/391220/ss_3b46aa127290e6ad2f62c125096bf5e901458ad6.600x338.jpg?t=1532513501");
        screens.add("https://steamcdn-a.akamaihd.net/steam/apps/391220/ss_2b612bfa62d920b061e900e1f7a975a2de6729ec.600x338.jpg?t=1532513501");
        game.setScreenshotsUrls(screens);
        game.setPrice(1299);
        adminService.addGame(game);
    }

    public void close(){
        HibernateUtil.close();
    }
}
