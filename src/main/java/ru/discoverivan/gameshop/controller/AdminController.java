package ru.discoverivan.gameshop.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import ru.discoverivan.gameshop.AppLoader;
import ru.discoverivan.gameshop.db.dao.*;
import ru.discoverivan.gameshop.db.entity.*;
import ru.discoverivan.gameshop.db.entity.additional.ExternalProduct;
import ru.discoverivan.gameshop.db.entity.additional.FullStats;
import ru.discoverivan.gameshop.db.entity.additional.ShortStats;
import ru.discoverivan.gameshop.domain.AdminService;
import ru.discoverivan.gameshop.domain.PaginationService;
import ru.discoverivan.gameshop.domain.StatsService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@RequestMapping("admin")
public class AdminController {

    private AppLoader appLoader;
    private AdminService adminService;
    private StatsService statsService;
    private PaginationService<Game> gamePaginationService;
    private GameDAO gameDAO;
    private OrderDAO orderDAO;
    private CustomerDAO customerDAO;
    private LanguageDAO languageDAO;
    private PlatformDAO platformDAO;
    private GenreDAO genreDAO;
    private AdminDAO adminDAO;
    private ProductDAO productDAO;

    @RequestMapping(value = "")
    public String showIndex() {
        return "admin/index";
    }

    @RequestMapping(value = "game")
    public String showGames(ModelMap model) {
        model.addAttribute("gameDAO", gameDAO);
        model.addAttribute("genreDAO", genreDAO);
        model.addAttribute("languageDAO", languageDAO);
        model.addAttribute("platformDAO", platformDAO);
        model.addAttribute("gamePaginationService", gamePaginationService);
        return "admin/game/game";
    }

    @RequestMapping(value = "game", method = RequestMethod.POST)
    public String gameAddHandler(
            @RequestParam(name = "game-add-submit") Object submit,
            @RequestParam(name = "title") String title,
            @RequestParam(name = "description") String description,
            @RequestParam(name = "poster-image-url") String posterImageUrl,
            @RequestParam(name = "languages") String[] languages,
            @RequestParam(name = "genres") String[] genres,
            @RequestParam(name = "platforms") String[] platforms,
            @RequestParam(name = "price") Float price,
            @RequestParam(name = "developer") String developer,
            @RequestParam(name = "release-date") String releaseDate,
            @RequestParam(name = "screenshot") String[] screenshots
    ) {
        if (submit != null){
            Game game = new Game();
            game.setTitle(title);
            game.setDescription(description);
            game.setPosterImageUrl(posterImageUrl);
            game.setGenres(convertGenreIdListToSet(genres));
            game.setLanguages(convertLanguagesIdListToSet(languages));
            game.setPlatforms(convertPlatformsIdListToSet(platforms));
            game.setPrice(price);
            game.setDeveloper(developer);
            game.setReleaseDate(parseDateFromString(releaseDate));
            game.setScreenshotsUrls(new HashSet<>(Arrays.asList(screenshots)));
            adminService.addGame(game);
        }
        return "redirect:/admin/game";
    }

    @RequestMapping(value = "game/remove", method = RequestMethod.POST)
    public String gameRemoveHandler(
            ModelMap model,
            @RequestParam(name = "game-delete") Object submit,
            @RequestParam(name = "id") Integer id
    ) {
        if (submit != null){
            adminService.deleteGame(id);
        }
        return "redirect:/admin/game";
    }

    @RequestMapping(value = "game/edit/{id}", method = RequestMethod.GET)
    public String showGameEdit(ModelMap model, @PathVariable(value = "id") int id) {
        model.addAttribute("urlParam1", id);
        model.addAttribute("gameDAO", gameDAO);
        model.addAttribute("genreDAO", genreDAO);
        model.addAttribute("languageDAO", languageDAO);
        model.addAttribute("platformDAO", platformDAO);
        return "admin/game/edit";
    }

    @RequestMapping(value = "game/edit/{id}", method = RequestMethod.POST)
    public String gameEditHandler(
            @RequestParam(name = "game-edit-submit") Object submit,
            @RequestParam(name = "id") Integer id,
            @RequestParam(name = "title") String title,
            @RequestParam(name = "description") String description,
            @RequestParam(name = "poster-image-url") String posterImageUrl,
            @RequestParam(name = "languages") String[] languages,
            @RequestParam(name = "genres") String[] genres,
            @RequestParam(name = "platforms") String[] platforms,
            @RequestParam(name = "price") Float price,
            @RequestParam(name = "developer") String developer,
            @RequestParam(name = "release-date") String releaseDate,
            @RequestParam(name = "screenshot") String[] screenshots
    ) {
        if (submit != null){
            Game game = new Game();
            game.setTitle(title);
            game.setDescription(description);
            game.setPosterImageUrl(posterImageUrl);
            game.setGenres(convertGenreIdListToSet(genres));
            game.setLanguages(convertLanguagesIdListToSet(languages));
            game.setPlatforms(convertPlatformsIdListToSet(platforms));
            game.setPrice(price);
            game.setDeveloper(developer);
            game.setReleaseDate(parseDateFromString(releaseDate));
            game.setScreenshotsUrls(new HashSet<>(Arrays.asList(screenshots)));
            adminService.editGame(id, game);
        }
        return "redirect:/admin/game";
    }

    @RequestMapping(value = "order", method = RequestMethod.GET)
    public String showOrders(ModelMap model) {
        model.addAttribute("orderDAO", orderDAO);
        return "admin/order/order";
    }

    @RequestMapping(value = "order", method = RequestMethod.POST)
    public String deleteOrder(
            @RequestParam(name="order-delete") Object submit,
            @RequestParam(name = "id") Integer id
    ) {
        if (submit != null){
            Order o = orderDAO.get(id);
            adminService.deleteOrder(id);
        }
        return "redirect:/admin/order";
    }

    @RequestMapping(value = "customer", method = RequestMethod.GET)
    public String showCustomers(ModelMap model) {
        model.addAttribute("customerDAO", customerDAO);
        return "admin/customer/customer";
    }

    @RequestMapping(value = "customer", method = RequestMethod.POST)
    public String deleteCustomer(
            @RequestParam(name="customer-delete") Object submit,
            @RequestParam(name = "id") Integer id
    ) {
        if (submit != null){
            adminService.deleteCustomer(id);
        }
        return "redirect:/admin/customer";
    }

    @RequestMapping(value = "stats")
    public String showStats(ModelMap model) {
        return "admin/stats/stats";
    }

    @RequestMapping(value = "stats/short", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ShortStats getShortStats(){
        return statsService.getShortStats();
    }

    @RequestMapping(value = "stats/full", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    FullStats getFullStats(@RequestParam(name = "time-interval") String timeInterval
    ){
        return statsService.getFullStats(timeInterval);
    }

    @RequestMapping(value = "language", method = RequestMethod.GET)
    public String showLanguages(ModelMap model) {
        model.addAttribute("languageDAO", languageDAO);
        return "admin/language/language";
    }

    @RequestMapping(value = "language", method = RequestMethod.POST)
    public String addLanguage(
            @RequestParam(name = "language-add-submit") Object submit,
            @RequestParam(name = "name") String name
    ) {
        if (submit != null){
            Language language = new Language();
            language.setName(name);
            adminService.addLanguage(language);
        }
        return "redirect:/admin/language";
    }

    @RequestMapping(value = "language/edit/{id}", method = RequestMethod.GET)
    public String showEditLanguage(ModelMap model, @PathVariable(name = "id") int id) {
        model.addAttribute("urlParam1", id);
        model.addAttribute("languageDAO", languageDAO);
        return "admin/language/edit";
    }

    @RequestMapping(value = "language/edit/{id}", method = RequestMethod.POST)
    public String editLanguageHandler(
            @RequestParam(name = "language-edit-submit") Object submit,
            @RequestParam(name = "id") Integer id,
            @RequestParam(name = "name") String name
    ) {
        if (submit != null){
            Language language = new Language();
            language.setName(name);
            adminService.editLanguage(id, language);
        }
        return "redirect:/admin/language";
    }

    @RequestMapping(value = "language/remove", method = RequestMethod.POST)
    public String deleteLanguage(
            @RequestParam(name="language-delete") Object submit,
            @RequestParam(name = "id") Integer id
    ) {
        if (submit != null){
            adminService.deleteLanguage(id);
        }
        return "redirect:/admin/language";
    }

    @RequestMapping(value = "genre", method = RequestMethod.GET)
    public String showGenres(ModelMap model) {
        model.addAttribute("genreDAO", genreDAO);
        return "admin/genre/genre";
    }

    @RequestMapping(value = "genre", method = RequestMethod.POST)
    public String addGenre(
            @RequestParam(name = "genre-add-submit") Object submit,
            @RequestParam(name = "name") String name
    ) {
        if (submit != null){
            Genre genre = new Genre();
            genre.setName(name);
            adminService.addGenre(genre);
        }
        return "redirect:/admin/genre";
    }

    @RequestMapping(value = "genre/edit/{id}", method = RequestMethod.GET)
    public String showEditGenre(ModelMap model, @PathVariable(name = "id") int id) {
        model.addAttribute("urlParam1", id);
        model.addAttribute("genreDAO", genreDAO);
        return "admin/genre/edit";
    }

    @RequestMapping(value = "genre/edit/{id}", method = RequestMethod.POST)
    public String editGenreHandler(
            @RequestParam(name = "genre-edit-submit") Object submit,
            @RequestParam(name = "id") Integer id,
            @RequestParam(name = "name") String name
    ) {
        if (submit != null){
            Genre genre = new Genre();
            genre.setName(name);
            adminService.editGenre(id, genre);
        }
        return "redirect:/admin/genre";
    }

    @RequestMapping(value = "genre/remove", method = RequestMethod.POST)
    public String deleteGenre(
            @RequestParam(name="genre-delete") Object submit,
            @RequestParam(name = "id") Integer id
    ) {
        if (submit != null){
            adminService.deleteGenre(id);
        }
        return "redirect:/admin/genre";
    }

    @RequestMapping(value = "platform", method = RequestMethod.GET)
    public String showPlatforms(ModelMap model) {
        model.addAttribute("platformDAO", platformDAO);
        return "admin/platform/platform";
    }

    @RequestMapping(value = "platform", method = RequestMethod.POST)
    public String addPlatform(
            @RequestParam(name = "platform-add-submit") Object submit,
            @RequestParam(name = "name") String name
    ) {
        if (submit != null){
            Platform platform = new Platform();
            platform.setName(name);
            adminService.addPlatform(platform);
        }
        return "redirect:/admin/platform";
    }

    @RequestMapping(value = "platform/edit/{id}", method = RequestMethod.GET)
    public String showEditPlatform(ModelMap model, @PathVariable(name = "id") int id) {
        model.addAttribute("urlParam1", id);
        model.addAttribute("platformDAO", platformDAO);
        return "admin/platform/edit";
    }

    @RequestMapping(value = "platform/edit/{id}", method = RequestMethod.POST)
    public String editPlatformHandler(
            @RequestParam(name = "platform-edit-submit") Object submit,
            @RequestParam(name = "id") Integer id,
            @RequestParam(name = "name") String name
    ) {
        if (submit != null){
            Platform platform = new Platform();
            platform.setName(name);
            adminService.editPlatform(id, platform);
        }
        return "redirect:/admin/platform";
    }

    @RequestMapping(value = "platform/remove", method = RequestMethod.POST)
    public String deletePlatform(
            @RequestParam(name="platform-delete") Object submit,
            @RequestParam(name = "id") Integer id
    ) {
        if (submit != null){
            adminService.deletePlatform(id);
        }
        return "redirect:/admin/platform";
    }

    private Date parseDateFromString(String s) {
        Date releaseDate = new Date();
        try {
            releaseDate = new SimpleDateFormat("yyyy-MM-dd").parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return releaseDate;
    }

    @RequestMapping(value = "settings", method = RequestMethod.GET)
    public String showSettings(ModelMap model) {
        model.addAttribute("adminDAO", adminDAO);
        return "admin/settings";
    }

    @RequestMapping(value = "product/import", method = RequestMethod.GET)
    public String showProductImport(ModelMap model) {
        model.addAttribute("adminDAO", adminDAO);
        model.addAttribute("gameDAO", gameDAO);
        return "admin/products/import";
    }

    @RequestMapping(value = "product/list", method = RequestMethod.GET)
    public String showProductList(ModelMap model) {
        model.addAttribute("adminDAO", adminDAO);
        model.addAttribute("gameDAO", gameDAO);
        return "admin/products/list";
    }

    @RequestMapping(value = "product/list", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Product> getProductList(
            @RequestParam(name = "game") Integer gameId) {
        System.out.println(gameDAO.get(gameId).getTitle());
        return productDAO.getAllProductsByGame(gameDAO.get(gameId));
    }

    @RequestMapping(value = "product/import", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Integer getExternalKeys(
            @RequestParam(name = "game") Integer gameId,
            @RequestParam(name = "url") String urlString) {
        String pageText = getPageContentFromUrl(urlString);
        List<ExternalProduct> products = getExternalProductsListFromPageContent(pageText);

        return products.size();
    }

    private List<ExternalProduct> getExternalProductsListFromPageContent(String pageText) {
        List<ExternalProduct> products = new ArrayList<>();
        ObjectMapper mapper = new ObjectMapper();
        try {
            products = mapper.readValue(pageText, mapper.getTypeFactory().constructCollectionType(List.class, ExternalProduct.class));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return products;
    }

    private String getPageContentFromUrl(@RequestParam(name = "url") String urlString) {
        String pageText = "";
        URL url = null;
        try {
            url = new URL(urlString);
            URLConnection conn = url.openConnection();
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8))) {
                pageText = reader.lines().collect(Collectors.joining("\n"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return pageText;
    }

    @RequestMapping(value = "settings", method = RequestMethod.POST)
    public String settingsHandler(
            @RequestParam(name = "base-init", required = false) Object baseInit,
            @RequestParam(name = "load-test-base", required = false) Object loadTestBase,
            @RequestParam(name = "count", required = false) Integer loadTestBaseCount
    ) {
        if (baseInit != null){
            appLoader.initBase();
        }
        if (loadTestBase != null){
            if (loadTestBaseCount > 100000){
                loadTestBaseCount = 100000;
            }
            appLoader.loadTest(loadTestBaseCount);
        }
        return "admin/settings";
    }

    @RequestMapping(value = "settings/admin/add", method = RequestMethod.POST, produces = MediaType.TEXT_PLAIN_VALUE)
    public @ResponseBody String addAdminAccount(@RequestParam(name = "username") String username,
                           @RequestParam(name = "password") String password,
                           @RequestParam(name = "password-confirm") String passwordConfirm
    ){
        if (!password.equals(passwordConfirm)) {
            return "0";
        }
        Admin admin = new Admin();
        admin.setUsername(username);
        admin.setPassword(password);
        adminService.registerNewAdmin(admin);
        return "1";
    }

    @RequestMapping(value = "settings/admin/delete", method = RequestMethod.POST, produces = MediaType.TEXT_PLAIN_VALUE)
    public @ResponseBody String removeAdminAccount(@RequestParam(name = "id") Integer id
    ){
        if (adminDAO.count() > 1) {
            adminService.removeAdmin(id);
            return "1";
        }
        return "0";
    }

    private Set<Platform> convertPlatformsIdListToSet(String[] platforms) {
        Set<Platform> platformSet = new HashSet<>();
        for (String platform : platforms) {
            platformSet.add(platformDAO.get(Integer.parseInt(platform)));
        }
        return platformSet;
    }

    private Set<Language> convertLanguagesIdListToSet(String[] languages) {
        Set<Language> languageSet = new HashSet<>();
        for (String language : languages) {
            languageSet.add(languageDAO.get(Integer.parseInt(language)));
        }
        return languageSet;
    }

    private Set<Genre> convertGenreIdListToSet(String[] genres) {
        Set<Genre> genresSet = new HashSet<>();
        for (String genre : genres) {
            genresSet.add(genreDAO.get(Integer.parseInt(genre)));
        }
        return genresSet;
    }

    @Autowired
    public void setAdminDAO(AdminDAO adminDAO) {
        this.adminDAO = adminDAO;
    }

    @Autowired
    public void setAdminService(AdminService adminService) {
        this.adminService = adminService;
    }

    @Autowired
    public void setAppLoader(AppLoader appLoader) {
        this.appLoader = appLoader;
    }

    @Autowired
    public void setStatsService(StatsService statsService) {
        this.statsService = statsService;
    }

    @Autowired
    public void setGamePaginationService(PaginationService<Game> gamePaginationService) {
        this.gamePaginationService = gamePaginationService;
    }

    @Autowired
    public void setGameDAO(GameDAO gameDAO) {
        this.gameDAO = gameDAO;
    }

    @Autowired
    public void setOrderDAO(OrderDAO orderDAO) {
        this.orderDAO = orderDAO;
    }

    @Autowired
    public void setCustomerDAO(CustomerDAO customerDAO) {
        this.customerDAO = customerDAO;
    }

    @Autowired
    public void setLanguageDAO(LanguageDAO languageDAO) {
        this.languageDAO = languageDAO;
    }

    @Autowired
    public void setPlatformDAO(PlatformDAO platformDAO) {
        this.platformDAO = platformDAO;
    }

    @Autowired
    public void setGenreDAO(GenreDAO genreDAO) {
        this.genreDAO = genreDAO;
    }

    @Autowired
    public void setProductDAO(ProductDAO productDAO) {
        this.productDAO = productDAO;
    }
}
