package ru.discoverivan.gameshop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.discoverivan.gameshop.db.dao.GameDAO;
import ru.discoverivan.gameshop.db.dao.GenreDAO;
import ru.discoverivan.gameshop.db.dao.OrderDAO;
import ru.discoverivan.gameshop.db.entity.Customer;
import ru.discoverivan.gameshop.db.entity.Order;
import ru.discoverivan.gameshop.db.entity.Product;
import ru.discoverivan.gameshop.domain.*;
import ru.discoverivan.gameshop.validation.RegisterForm;
import ru.discoverivan.gameshop.validation.RegisterFormValidator;
import ru.discoverivan.gameshop.validation.ValidationResult;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping(value = "")
public class ShopController {

    private ShopService shopService;
    private CartService cartService;
    private UserService userService;
    private SecurityService securityService;
    private RegisterFormValidator registerFormValidator;
    private GameDAO gameDAO;
    private GenreDAO genreDAO;
    private OrderDAO orderDAO;

    @RequestMapping(value = "")
    public String showIndex(ModelMap model) {
        model.addAttribute("gameDAO", gameDAO);
        model.addAttribute("genreDAO", genreDAO);

        return "site/index";
    }

    @RequestMapping(value = "account")
    public String showAccountPage(ModelMap model) {
        model.addAttribute("userService", userService);
        model.addAttribute("orderDAO", orderDAO);
        return "site/account/index";
    }

    @RequestMapping(value = "account/order/{id}")
    public String showAccountOrderPage(ModelMap model, @PathVariable(value = "id") int id) {
        model.addAttribute("urlParam1", id);
        model.addAttribute("orderDAO", orderDAO);
        model.addAttribute("userService", userService);
        return "site/account/order";
    }

    @RequestMapping(value = "login", method = RequestMethod.GET)
    public String showLoginPage() {
        return "site/login";
    }

    @RequestMapping(value = "register", method = RequestMethod.GET)
    public ModelAndView showRegisterPage(Model model) {
        return new ModelAndView("site/register","registerForm", new RegisterForm());
    }

    @RequestMapping(value = "register", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ValidationResult processRegister(
            @ModelAttribute("register") RegisterForm registerForm
    ) {
        ValidationResult validationResult = registerFormValidator.validate(registerForm);
        if (validationResult.getSuccess()){
            Customer customer = new Customer();
            customer.setName(registerForm.getName());
            customer.setEmail(registerForm.getEmail());
            customer.setUsername(registerForm.getUsername());
            customer.setPassword(registerForm.getPassword());
            userService.registerNewCustomer(customer);

            securityService.autoLogin(registerForm.getUsername(), registerForm.getPassword());
        }
        return validationResult;
    }

    @RequestMapping(value = "/game/{id}", method = RequestMethod.GET)
    public String showFullGame(ModelMap model,
                               @CookieValue(value = "cart", required = false) String cartCookie,
                               @PathVariable(value = "id") int id) {
        model.addAttribute("cart", cartService.getCartFromCookie(cartCookie));
        model.addAttribute("urlParam1", id);
        model.addAttribute("gameDAO", gameDAO);
        model.addAttribute("shopService", shopService);
        return "site/game-full";
    }

    @RequestMapping(value = "/game/{id}", method = RequestMethod.POST)
    public String addGameToCart(HttpServletResponse response,
                                @CookieValue(value = "cart",required = false) String cartCookie,
                                @PathVariable(value = "id") int id) {
        cartCookie = cartService.addGameToCartCookie(cartCookie, id);
        setCartCookie(response, cartCookie);
        return "redirect:/game/"+id;
    }

    @RequestMapping(value = "/cart", method = RequestMethod.GET)
    public String showCartPage(ModelMap model,
                               HttpServletResponse response,
                               @CookieValue(value = "cart", required = false) String cartCookie) {
        Set<Integer> cart = cartService.getCartFromCookie(cartCookie);
        cart = cartService.validateCart(cart);
        setCartCookie(response, cartService.getCookieFromCart(cart));
        model.addAttribute("cart", cart);
        model.addAttribute("gameDAO", gameDAO);
        return "site/cart";
    }

    private void setCartCookie(HttpServletResponse response, String cookieFromCart) {
        Cookie cookie = new Cookie("cart", cookieFromCart);
        cookie.setMaxAge(3024000);
        cookie.setPath("/");
        response.addCookie(cookie);
    }

    @RequestMapping(value = "/cart", method = RequestMethod.POST)
    public String deleteFromCart(HttpServletResponse response,
                                 @CookieValue(value = "cart", required = false) String cartCookie,
                                 @RequestParam(name = "game-id") Integer id) {
        cartCookie = cartService.removeGameFromCartCookie(cartCookie, id);
        setCartCookie(response, cartCookie);
        return "redirect:/cart";
    }

    @RequestMapping(value = "/buy", method = RequestMethod.POST)
    public String processOrder(
            HttpServletResponse response,
            @CookieValue(value = "cart", required = false) String cartCookie,
            Principal principal
    ) {
        Set<Integer> cartGames = cartService.getCartFromCookie(cartCookie);
        Customer customer = userService.findByUsername(principal.getName());
        Order order = shopService.createOrder(cartGames, customer);
        shopService.processOrder(order);
        setCartCookie(response, "");
        return "site/buy-success";
    }

//    @RequestMapping(value = "/test", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseBody
//    public List<Product> generateProductList(){
//        List<Product> products = new ArrayList<>();
//        for (int i=0; i<1000; i++){
//            Product p = new Product();
//            p.setId(i);
//            p.setKey("license key");
//            products.add(p);
//        }
//        return products;
//    }

    @Autowired
    public void setRegisterFormValidator(RegisterFormValidator registerFormValidator) {
        this.registerFormValidator = registerFormValidator;
    }

    @Autowired
    public void setCartService(CartService cartService) {
        this.cartService = cartService;
    }

    @Autowired
    public void setOrderDAO(OrderDAO orderDAO) {
        this.orderDAO = orderDAO;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }

    @Autowired
    public void setShopService(ShopService shopService) {
        this.shopService = shopService;
    }

    @Autowired
    public void setGameDAO(GameDAO gameDAO) {
        this.gameDAO = gameDAO;
    }

    @Autowired
    public void setGenreDAO(GenreDAO genreDAO) {
        this.genreDAO = genreDAO;
    }
}
