package ru.discoverivan.gameshop.servelets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ErrorHandler extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Throwable throwable = (Throwable) request.getAttribute("javax.servlet.error.exception");
        Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
        String servletName = (String) request.getAttribute("javax.servlet.error.servlet_name");
        if (servletName == null)
            servletName = "Unknown";
        String requestUri = (String) request.getAttribute("javax.servlet.error.request_uri");
        if (requestUri == null)
            requestUri = "Unknown";

        if(statusCode == null){
            response.sendRedirect("/");
            return;
        }

        request.setAttribute("statusCode", statusCode);
        request.setAttribute("requestUri", requestUri);
        request.getRequestDispatcher("/WEB-INF/views/site/error.jsp").forward(request, response);
    }
}
