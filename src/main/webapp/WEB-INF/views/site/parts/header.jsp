<%@ page import="ru.discoverivan.gameshop.domain.CartService" %>
<%@ page import="java.io.IOException" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="gshop" uri="gshop" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:include page="/WEB-INF/views/site/parts/head-includes.jsp"/>

<body id="index">
<header>
    <div class="container">
        <div class="row bar">
            <div class="col-12">
                <a href="/" class="title">GAMESHOP</a>
                <div class="links">
                    <a href="#">Новинки</a>
                    <a href="#">Популярное</a>
                </div>
                <div class="right">
                    <a href="/cart" class="admin">
                        <div class="name">КОРЗИНА (<gshop:getCartNum cartCookie="${cookie.cart.value}"/>)</div>
                    <security:authorize access="isAnonymous()">
                        <a href="/register" class="admin">РЕГИСТРАЦИЯ</a>
                        <a href="/login" class="admin">ВХОД</a>
                    </security:authorize>
                    <security:authorize access="hasAuthority('ADMIN')">
                        <a href="/admin" class="admin">ПАНЕЛЬ УПРАВЛЕНИЯ</a>
                    </security:authorize>
                    <security:authorize access="hasAuthority('CUSTOMER')">
                        <a href="/account" class="admin">ЛИЧНЫЙ КАБИНЕТ</a>
                    </security:authorize>
                    <security:authorize access="isAuthenticated()">
                        <a href="/logout" class="admin">ВЫЙТИ</a>
                    </security:authorize>
                </div>
            </div>
        </div>
    </div>
</header>