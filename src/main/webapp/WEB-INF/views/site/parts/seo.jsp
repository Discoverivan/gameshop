<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:choose>
    <c:when test="${!empty pageTitle}">
        <title>${pageTitle} | GAMESHOP - магазин игр</title>

    </c:when>
    <c:otherwise>
        <title>GAMESHOP - магазин игр</title>
        <meta name="description"  content="Магазин компьютерных игр" />
    </c:otherwise>
</c:choose>

<c:choose>
    <c:when test="${!empty pageDescription}">
        <meta name="description"  content="${pageDescription}" />
    </c:when>
    <c:otherwise>
        <meta name="description"  content="Магазин компьютерных игр" />
    </c:otherwise>
</c:choose>




