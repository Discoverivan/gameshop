<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:include page="/WEB-INF/views/site/parts/head-includes.jsp"/>

<body id="index">
    <section id="error">
        <div class="container">
            <div class="row">
                <div class="col-12 wrapper">
                    <div class="error-block">
                        <img src="/images/icon.png">
                        <div class="code">${statusCode}</div>
                        <div class="description">
                            <c:choose>
                                <c:when test="${statusCode == 404}">
                                    Запрашиваемая страница не найдена
                                </c:when>
                                <c:when test="${statusCode == 403}">
                                    У вас нет доступа
                                </c:when>
                                <c:when test="${statusCode == 500}">
                                    Внутренняя ошибка сервера
                                </c:when>
                                <c:otherwise>
                                    Неизвестная ошибка
                                </c:otherwise>
                            </c:choose>
                        </div>
                        <div class="back">
                            <a class="main-button" href="/">Перейти на главную страницу</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<jsp:include page="/WEB-INF/views/site/parts/footer.jsp"/>
