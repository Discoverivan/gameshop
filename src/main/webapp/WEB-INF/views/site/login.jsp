<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:set var="pageTitle" value="Авторизация" scope="request"/>
<c:set var="pageDescription" value="Вход на сайт" scope="request"/>
<jsp:include page="/WEB-INF/views/site/parts/head-includes.jsp"/>

<security:authorize access="isAuthenticated()">
    <% response.sendRedirect("/"); %>
</security:authorize>

<body id="login">
<form id="login-form" class="form-signin" method="post" action="/login">
    <a href="/"><img class="icon" src="/images/icon.png" alt="" width="100" height="100"></a>
    <h1>Авторизация</h1>

    <input type="text" name="username" class="form-control" placeholder="Имя пользователя" required autofocus>
    <input type="password" name="password" class="form-control" placeholder="Пароль" required>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <button class="main-button main-button-block" type="submit">Войти</button>
    <c:choose>
        <c:when test="${param.error == 1}">
            <div class="alert alert-danger" role="alert">
                Неправильный логин или пароль
            </div>
        </c:when>
        <c:when test="${param.logout == 1}">
            <div class="alert alert-primary" role="alert">
                Вы успешно вышли из системы
            </div>
        </c:when>
    </c:choose>
    <a class="back" href="/register">Зарегистрироваться</a>
</form>

<jsp:include page="/WEB-INF/views/site/parts/footer.jsp"/>