<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:set var="pageTitle" value="Авторизация" scope="request"/>
<c:set var="pageDescription" value="Вход на сайт" scope="request"/>
<jsp:include page="/WEB-INF/views/site/parts/head-includes.jsp"/>

<security:authorize access="isAuthenticated()">
    <% response.sendRedirect("/"); %>
</security:authorize>

<body id="register">
<form:form id="login-form" class="form-signin register-form" method="post" modelAttribute="registerForm">
    <a href="/"><img class="icon" src="/images/icon.png" alt="" width="100" height="100"></a>
    <h1>Регистрация покупателя</h1>
    <form:input type="text" path="name" name="name"
                class="form-control" placeholder="Ваше имя" required="required" autofocus="autofocus"
                data-toggle="tooltip" data-placement="right" data-trigger="manual"/>
    <form:input type="email" name="email" path="email"
                class="form-control" placeholder="Электронная почта" required="required"
                data-toggle="tooltip" data-placement="right" data-trigger="manual"/>
    <form:input type="text" name="username" path="username"
                class="form-control" placeholder="Имя пользователя" required="required"
                data-toggle="tooltip" data-placement="right" data-trigger="manual"/>
    <form:input type="password" name="password" path="password"
                class="form-control" placeholder="Пароль" required="required"
                data-toggle="tooltip" data-placement="right" data-trigger="manual"/>
    <form:input type="password" name="passwordConfirm" path="passwordConfirm"
                class="form-control" placeholder="Подтверждение пароля" required="required"
                data-toggle="tooltip" data-placement="right" data-trigger="manual"/>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <button class="main-button main-button-block" type="submit">Создать аккаунт</button>
    <div class="alert alert-danger" role="alert" style="display: none;">
        Что-то пошло не так...
    </div>
    <a class="back" href="/login">Уже зарегистрированы?</a>
</form:form>

<jsp:include page="/WEB-INF/views/site/parts/footer.jsp"/>