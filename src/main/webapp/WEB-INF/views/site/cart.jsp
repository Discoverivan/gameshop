<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:include page="/WEB-INF/views/site/parts/header.jsp"/>

<section id="order">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>Корзина:</h1>
                <table>
                    <c:choose>
                        <c:when test="${fn:length(cart)>0}">
                            <c:set var="total" value="${0}"/>
                            <c:forEach items="${cart}" var="gameId" >
                                <c:set var="game" value='${gameDAO.get(gameId)}' />
                                <c:set var="total" value="${total-price + game.price}"/>
                                <tr>
                                    <td class="image"><img src="${game.posterImageUrl}" alt="${game.title}"></td>
                                    <td class="title">${game.title}</td>
                                    <td class="price">
                                        <fmt:formatNumber type = "number" maxFractionDigits="0" value = "${game.price}" /> ₽
                                    </td>
                                    <td class="delete">
                                        <form method="post">
                                            <input type="hidden" name="game-id" value="${gameId}">
                                            <button type="submit" class="main-button">Удалить</button>
                                        </form>
                                    </td>
                                </tr>
                            </c:forEach>
                        </c:when>
                        <c:otherwise>
                            <tr>
                                <th colspan="4" scope="row">ТОВАРОВ В КОРЗИНЕ НЕТ</th>
                            </tr>
                        </c:otherwise>
                    </c:choose>
                </table>
                <c:choose>
                    <c:when test="${fn:length(cart)>0}">
                        <div class="order-footer row">
                            <div class="col-md-6 back">
                                <a href="/">Вернуться к покупкам</a>
                            </div>
                            <div class="col-md-6 total">
                                <div class="label">Итого к оплате: </div>
                                <div class="price">
                                    <fmt:formatNumber type = "number" maxFractionDigits="0" value = "${total}" /> ₽
                                </div>
                            </div>
                        </div>
                        <div class="input-block">
                            <form action="/buy" method="post">
                                <button class="main-button" type="submit" name="order-submit">ПЕРЕЙТИ К ОПЛАТЕ</button>
                            </form>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <div class="row buy-something">
                            <div class="col-12">
                                <a href="/" class="main-button">Парейти на главную страницу</a>
                            </div>
                        </div>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>
</section>

<jsp:include page="/WEB-INF/views/site/parts/footer.jsp"/>
