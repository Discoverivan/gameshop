<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:include page="/WEB-INF/views/site/parts/header.jsp"/>

<c:if test="${order-success != '1'}">
    <%--<c:redirect url = "/"/>--%>
    redirect

</c:if>
${order-success}

<section id="order">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>Заказ успешно совершен!</h1>
                <div class="success">
                    <p>Проверьте вашу почту. На нее должен прийти ключ продукта.</p>
                    <a class="main-button" href="/">Вернуться к покупкам</a>
                </div>
            </div>
        </div>
    </div>
</section>

<jsp:include page="/WEB-INF/views/site/parts/footer.jsp"/>
