<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:include page="/WEB-INF/views/site/parts/header.jsp"/>

<section class="banners">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div id="banners-slider" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <a href="#" class="banner" style="background-image: url('https://i.imgur.com/la3Uveg.jpg')">
                                <div class="label-wrap">
                                    <div class="label primary">Tomb Raider</div>
                                    <div class="label secondary">Купить сейчас</div>
                                </div>
                            </a>
                        </div>
                        <div class="carousel-item">
                            <a href="#" class="banner" style="background-image: url('https://www.gamewallpapers.com/img_script/wallpaper_dir/img.php?src=wallpaper_far_cry_5_06_2560x1080.jpg&height=506')">
                                <div class="label-wrap">
                                    <div class="label primary">Far Cry 5</div>
                                    <div class="label secondary">Купить сейчас</div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="games-list">
    <div class="container">
        <div class="row">
        <div class="col-12">
            <a href="#" class="category-title">Новинки</a>
        </div>
    </div>
        <div class="row cards">
            <c:forEach items="${gameDAO.getOrderedList('release_date', 4)}" var="game">
                <div class="col-lg-3">
                    <div class="game-card" style="background-image: url('${game.posterImageUrl}')">
                        <div class="title">${game.title}</div>
                        <div class="bottom">
                            <div class="price">
                                <fmt:formatNumber type = "number" maxFractionDigits="0" value = "${game.price}" /> ₽
                            </div>
                            <a href="game/${game.id}" class="main-button">КУПИТЬ</a>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>

        <div class="row">
            <div class="col-12">
                <a href="#" class="category-title">Самые популярные</a>
            </div>
        </div>
        <div class="row cards">
            <c:forEach items="${gameDAO.getOrderedList('views', 4)}" var="game">
                <div class="col-lg-3">
                    <div class="game-card" style="background-image: url('${game.posterImageUrl}')">
                        <div class="title">${game.title}</div>
                        <div class="bottom">
                            <div class="price">
                                <fmt:formatNumber type = "number" maxFractionDigits="0" value = "${game.price}" /> ₽
                            </div>
                            <a href="game/${game.id}" class="main-button">КУПИТЬ</a>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>

        <%--<div class="row">--%>
            <%--<div class="col-12">--%>
                <%--<a href="#" class="category-title">Игры в жанре Action</a>--%>
            <%--</div>--%>
        <%--</div>--%>
        <%--<div class="row cards">--%>
            <%--<c:forEach items="${gameDAO.selectListByGenre(genreDAO.get(2),4)}" var="game">--%>
                <%--<div class="col-lg-3">--%>
                    <%--<div class="game-card" style="background-image: url('${game.posterImageUrl}')">--%>
                        <%--<div class="title">${game.title}</div>--%>
                        <%--<div class="bottom">--%>
                            <%--<div class="price">--%>
                                <%--<fmt:formatNumber type = "number" maxFractionDigits="0" value = "${game.price}" /> ₽--%>
                            <%--</div>--%>
                            <%--<a href="game/${game.id}" class="main-button">КУПИТЬ</a>--%>
                        <%--</div>--%>
                    <%--</div>--%>
                <%--</div>--%>
            <%--</c:forEach>--%>
        <%--</div>--%>
    </div>
</section>

<jsp:include page="/WEB-INF/views/site/parts/footer.jsp"/>
