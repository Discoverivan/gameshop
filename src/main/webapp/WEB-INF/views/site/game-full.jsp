<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:set var="game" value='${gameDAO.get(urlParam1)}' scope="request" />
<c:set var="pageTitle" value="${game.title}" scope="request"/>
<c:set var="pageDescription" value="${game.description}" scope="request"/>

<jsp:include page="/WEB-INF/views/site/parts/header.jsp"/>

<c:if test="${empty game}">
    <c:redirect url = "/error404"/>
</c:if>
${shopService.gameViewed(game.id)}
<section id="game-full">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="poster">
                    <img src="${game.posterImageUrl}" alt="${game.title}">
                </div>
                <div class="price">
                    <fmt:formatNumber type = "number" maxFractionDigits="0" value = "${game.price}" /> ₽
                </div>
                <c:choose>
                    <c:when test="${fn:contains(cart, game.id)}">
                        <a href="/cart" class="buy main-button main-button-block">Перейти в корзину</a>
                    </c:when>
                    <c:otherwise>
                        <form method="post" class="buy-button">
                            <button type="submit" class="buy main-button main-button-block">Купить сейчас</button>
                        </form>
                    </c:otherwise>
                </c:choose>
            </div>
            <div class="col-lg-8">
                <div class="game-info">
                    <h1 class="game-title">${game.title}</h1>
                    <table class="tech-info">
                        <tr>
                            <td class="name">Платформа:</td>
                            <td class="value">
                                <c:forEach items="${game.platforms}" var="platform">
                                    <span class="badge badge-pill badge-secondary">${platform.name}</span>
                                </c:forEach>
                            </td>
                        </tr>
                        <tr>
                            <td class="name">Разработчик:</td>
                            <td class="value">${game.developer}</td>
                        </tr>
                        <tr>
                            <td class="name">Дата выхода:</td>
                            <td class="value">
                                <fmt:formatDate pattern = "dd.MM.yyyy" value = "${game.releaseDate}" />
                            </td>
                        </tr>
                        <tr>
                            <td class="name">Просмотров:</td>
                            <td class="value">
                                ${game.views}
                            </td>
                        </tr>
                        <tr>
                            <td class="name">Жанр:</td>
                            <td class="value">
                                <c:forEach items="${game.genres}" var="genre">
                                    <span class="badge badge-pill badge-secondary">${genre.name}</span>
                                </c:forEach>
                            </td>
                        </tr>
                        <tr>
                            <td class="name">Язык:</td>
                            <td class="value">
                                <c:forEach items="${game.languages}" var="language">
                                    <span class="badge badge-pill badge-secondary">${language.name}</span>
                                </c:forEach>
                            </td>
                        </tr>
                    </table>
                    <h2>Описание:</h2>
                    <p class="game-description">${game.description}</p>

                    <c:if test="${!empty game.screenshotsUrls}">
                        <h2>Скриншоты:</h2>
                        <div class="row screenshots">
                        <c:forEach items="${game.screenshotsUrls}" var="screenshot">
                            <div class="col-lg-6">
                                <img src="${screenshot}" alt="${game.title}">
                            </div>
                        </c:forEach>
                    </c:if>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<jsp:include page="/WEB-INF/views/site/parts/footer.jsp"/>
