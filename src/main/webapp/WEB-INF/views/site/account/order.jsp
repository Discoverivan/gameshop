<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:include page="/WEB-INF/views/site/parts/header.jsp"/>

<security:authentication var="principal" property="principal" />
<c:set var="user" value="${userService.findByUsername(principal.username)}"/>
<c:set var="order" value="${orderDAO.get(urlParam1)}"/>

<c:set var="hasAccess" value="false" />
<c:forEach var="item" items="${user.orders}">
    <c:if test="${item.id == order.id}">
        <c:set var="hasAccess" value="true" />
    </c:if>
</c:forEach>

<c:if test="${empty order || !hasAccess}">
    <c:redirect url = "/error404"/>
</c:if>

<section id="account-index">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>Заказ №${order.id} (от <fmt:formatDate pattern = "dd.MM.yyyy HH:mm:ss" value = "${order.date}" />)</h1>
                <div class="order-details">
                    <h3>Детали заказа:</h3>
                    <table>
                        <c:forEach items="${order.products}" var="product">
                            <tr>
                                <td class="image"><img src="${product.game.posterImageUrl}" alt="${product.game.title}"></td>
                                <td class="title">${product.game.title}</td>
                                <td class="price">
                                    <fmt:formatNumber type = "number" maxFractionDigits="0" value = "${product.game.price}" /> ₽
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <a class="main-button" href="/account">Вернуться назад</a>
            </div>
        </div>
    </div>
</section>

<jsp:include page="/WEB-INF/views/site/parts/footer.jsp"/>