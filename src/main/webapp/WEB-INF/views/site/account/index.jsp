<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:include page="/WEB-INF/views/site/parts/header.jsp"/>

<security:authentication var="principal" property="principal" />
<c:set var="user" value="${userService.findByUsername(principal.username)}"/>

<section id="account-index">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>Привет, ${user.name}!</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <h3>Список ваших заказов:</h3>
                <table class="table table-dark">
                    <thead>
                    <tr>
                        <th scope="col">Дата</th>
                        <th scope="col">Номер заказа</th>
                        <th scope="col">Игры в заказе</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:set var="orders" value='${user.orders}' />
                    <c:forEach items="${orders}" var="order">
                        <tr>
                            <td><fmt:formatDate pattern = "dd.MM.yyyy HH:mm:ss" value = "${order.date}" /></td>
                            <td>${order.id}</td>
                            <td>
                                <c:forEach items="${order.products}" var="product">
                                    ${product.game.title}<br>
                                </c:forEach>
                            </td>
                            <td class="buttons">
                                <a class="main-button" href="/account/order/${order.id}">Подробнее</a>
                            </td>
                        </tr>
                    </c:forEach>
                    <c:if test="${empty orders}">
                        <tr>
                            <th colspan="5" scope="row">ЗАКАЗОВ НЕТ</th>
                        </tr>
                    </c:if>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

<jsp:include page="/WEB-INF/views/site/parts/footer.jsp"/>

