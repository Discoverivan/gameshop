<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:include page="/WEB-INF/views/admin/parts/header.jsp"/>

<section id="admin-index">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <a href="/admin/game/" class="button-block">
                    <img src="/images/admin/game.svg">
                    <p>Игры</p>
                </a>
            </div>
            <div class="col-md-4">
                <a href="/admin/order/" class="button-block">
                    <img src="/images/admin/orders.svg">
                    <p>Заказы</p>
                </a>
            </div>
            <div class="col-md-4">
                <a href="/admin/customer/" class="button-block">
                    <img src="/images/admin/clients.svg">
                    <p>Покупатели</p>
                </a>
            </div>
            <div class="col-md-4">
                <a href="/admin/stats/" class="button-block">
                    <img src="/images/admin/stats.svg">
                    <p>Статистика</p>
                </a>
            </div>
            <div class="col-md-4">
                <a href="/admin/settings/" class="button-block">
                    <img src="/images/admin/settings.svg">
                    <p>Настройки</p>
                </a>
            </div>
        </div>
    </div>
</section>

<jsp:include page="/WEB-INF/views/admin/parts/footer.jsp"/>