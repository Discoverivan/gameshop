<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

    <jsp:include page="/WEB-INF/views/admin/parts/header.jsp"/>

    <c:set var="genre" value='${genreDAO.get(urlParam1)}' />
  <div class="container">
    <div class="row edit-list">
      <div class="col-12">
        <h2>Редактирование жанра:</h2>
        <form method="post">
          <input type="hidden" name="id" value="${genre.id}">
          <div class="form-group">
            <label>Название: </label>
            <input type="text" class="form-control" name="name" value="${genre.name}" required>
          </div>
          <button type="submit" name="genre-edit-submit" class="btn btn-primary">Завершить редактирование</button>
          <a class="btn btn-secondary" href="/admin/genre/">Назад</a>
        </form>
      </div>
    </div>
</div>

<jsp:include page="/WEB-INF/views/admin/parts/footer.jsp"/>
