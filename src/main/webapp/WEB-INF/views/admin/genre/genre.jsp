<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:include page="/WEB-INF/views/admin/parts/header.jsp"/>

<div class="container">
    <div class="row edit-list">
      <div class="col-12">
        <h2>Список всех жанров:</h2>
        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th>ID</th>
              <th>Название</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
          <c:set var="genres" value='${genreDAO.getAll()}' />
          <c:forEach items="${genres}" var="genre">
          <tr>
            <th scope="row">${genre.id}</th>
            <td>${genre.name}</td>
            <td class="buttons">
              <a class="btn btn-warning" href="/admin/genre/edit/${genre.id}">Редактировать</a>
              <form class="remove-form" method="post" action="/admin/genre/remove">
                <input type="hidden" name="id" value="${genre.id}">
                <button type="submit" name="genre-delete" class="btn btn-danger">Удалить</button>
              </form>
            </td>
          </tr>
          </c:forEach>
          <c:if test="${empty genres}">
          <tr>
            <th colspan="3" scope="row"><% out.print("ПОКА НИЧЕГО НЕ ЗАПОЛНЕНО"); %></th>
          </tr>
          </c:if>
          </tbody>
        </table>
      </div>
    </div>
    <div class="row edit-list">
      <div class="col-12">
        <h2>Добавить жанр:</h2>
        <form method="post">
          <div class="form-group">
            <label>Название: </label>
            <input type="text" class="form-control" name="name" required>
          </div>
          <button type="submit" name="genre-add-submit" class="btn btn-primary">Добавить жанр</button>
        </form>
      </div>
    </div>
</div>

<jsp:include page="/WEB-INF/views/admin/parts/footer.jsp"/>
