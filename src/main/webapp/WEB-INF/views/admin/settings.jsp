<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:include page="/WEB-INF/views/admin/parts/header.jsp"/>

<div id="admin-settings"class="container">
    <div class="row edit-list">
       <div class="col-12">
           <h1>Инициализация базы:</h1>
           <form method="post">
           <button type="submit" class="btn btn-warning" name="base-init">Инициализировать</button>
           </form>
       </div>
    </div>
    <div class="row edit-list">
        <div class="col-12">
            <h1>Нагрузочный тест базы с играми:</h1>
            <form method="post">
                <div class="form-group">
                    <label>Количество записей: </label>
                    <input type="number" min="1" max="100000" class="form-control" name="count" value="1000" required>
                </div>
                <button type="submit" class="btn btn-warning" name="load-test-base">Заполнить базу</button>
            </form>
        </div>
    </div>
    <div class="row edit-list admin-list">
        <div class="col-12">
            <h2>Список всех администраторов:</h2>
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th>ID</th>
                    <th>Имя пользователя</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <c:set var="admins" value='${adminDAO.getAll()}' />
                <c:forEach items="${admins}" var="admin">
                    <tr>
                        <th scope="row">${admin.id}</th>
                        <td>${admin.username}</td>
                        <td class="buttons">
                            <form class="remove-admin" method="post" action="/admin/settings/admin/delete">
                                <input type="hidden" name="id" value="${admin.id}">
                                <button type="submit" name="customer-delete" class="btn btn-danger">Удалить</button>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
            <div class="alert alert-danger" style="display:none;">Что-то пошло не так...</div>
        </div>
    </div>
    <div class="row edit-list">
    <div class="col-12">
        <h1>Добавить администратора:</h1>
        <form class="add-admin" method="post" action="/admin/settings/admin/add">
            <div class="form-group">
                <label>Имя пользователя: </label>
                <input type="text" class="form-control" name="username" required>
            </div>
            <div class="form-group">
                <label>Пароль: </label>
                <input type="password" class="form-control" name="password" required>
            </div>
            <div class="form-group">
                <label>Пароль еще раз: </label>
                <input type="password" class="form-control" name="password-confirm" required>
            </div>
            <button type="submit" class="btn btn-primary">Добавить администратора</button>
        </form>
        <div class="alert alert-danger" style="display: none;">Что то пошло не так...</div>
        <div class="alert alert-success" style="display: none;">Администратор добавлен!</div>
    </div>
</div>
</div>

<jsp:include page="/WEB-INF/views/admin/parts/footer.jsp"/>
