<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:include page="/WEB-INF/views/admin/parts/header.jsp"/>

<section id="admin-stats">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Основная статистика магазина:</h2>
            </div>
        </div>
        <div class="row load" id="load-short-stats">
            <div class="col-12">
                <img src="/images/admin/loading.svg" width="100">
            </div>
        </div>
        <div id="short-stats-block" class="row" style="display: none;">
            <div class="col-md-4">
                <div id="all-games" class="stats-block">
                    <div class="number">0</div>
                    <p class="label">Всего игр</p>
                </div>
            </div>
            <div class="col-md-4">
                <div id="all-orders" class="stats-block">
                    <div class="number">0</div>
                    <p class="label">Всего заказов</p>
                </div>
            </div>
            <div class="col-md-4">
                <div id="all-customers" class="stats-block">
                    <div class="number">0</div>
                    <p class="label">Всего покупателей</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-8">
                <h2>Подробная статистика:</h2>
            </div>
            <div class="col-4">
                <select id="time-interval" name="time-interval" class="form-control" onchange="changeTimeIntervalOnFullStats()">
                    <option value="today">сегодня</option>
                    <option value="7">последние 7 дней</option>
                    <option value="30">последние 30 дней</option>
                    <option value="90">последние 90 дней</option>
                    <option value="180">последние полгода</option>
                    <option value="all">за все время</option>
                </select>
            </div>
        </div>
        <div class="row load" id="load-full-stats">
            <div class="col-12">
                <img src="/images/admin/loading.svg" width="100">
            </div>
        </div>
        <div class="row" id="full-stats-block" style="display: none;">
            <div class="col-md-4">
                <div id="orders-count" class="stats-block">
                    <div class="number">0</div>
                    <p class="label">заказов</p>
                </div>
            </div>
            <div class="col-md-4">
                <div id="orders-sum" class="stats-block">
                    <div class="number">0</div>
                    <p class="label">сумма по заказам</p>
                </div>
            </div>
            <div class="col-12">
                <canvas id="ordersGraph" height="100"></canvas>
            </div>
        </div>
    </div>
</section>
<jsp:include page="/WEB-INF/views/admin/parts/footer.jsp"/>
