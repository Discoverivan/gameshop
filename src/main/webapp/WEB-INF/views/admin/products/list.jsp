<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:include page="/WEB-INF/views/admin/parts/header.jsp"/>
<c:set var="games" value='${gameDAO.getAll()}' scope="session" />

<div id="admin-product-list"class="container">
    <div class="row edit-list">
        <div class="col-12">
            <h1>Список ключей: </h1>
            <form class="external-form" method="post">
                <div class="form-group">
                    <label>Выберите игру: </label>
                    <select class="form-control" name="game">
                        <c:forEach var="game" items="${games}">
                            <option value="${game.id}">${game.title}</option>
                        </c:forEach>
                    </select>
                </div>
                <button type="submit" class="btn btn-warning" name="submit">Получить список ключей</button>
            </form>
            <div class="alert alert-success" style="display: none"></div>
        </div>
    </div>
</div>

<jsp:include page="/WEB-INF/views/admin/parts/footer.jsp"/>