<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:include page="/WEB-INF/views/admin/parts/header.jsp"/>
<c:set var="games" value='${gameDAO.getAll()}' scope="session" />

<div id="admin-product-import"class="container">
    <div class="row edit-list">
        <div class="col-12">
            <h1>Получить данные с внешнего сервера: </h1>
            <form class="external-form" method="post">
                <div class="form-group">
                    <label>Выберите игру, в которую будут импортированы ключи:</label>
                    <select class="form-control" name="game">
                        <c:forEach var="game" items="${games}">
                            <option value="${game.id}">${game.title}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="form-group">
                    <label>Ссылка на API поставщика:</label>
                    <input class="form-control" type="text" name="url" value="https://discoverivan.ru/project/gameshop/?game_id=1&keys_num=10">
                </div>
                <button type="submit" class="btn btn-warning" name="base-init">Получить</button>
            </form>
            <div class="alert alert-success" style="display: none"></div>
        </div>
    </div>
</div>

<jsp:include page="/WEB-INF/views/admin/parts/footer.jsp"/>