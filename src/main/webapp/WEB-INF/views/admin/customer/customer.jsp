<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:include page="/WEB-INF/views/admin/parts/header.jsp"/>

<div class="container">
    <div class="row edit-list">
        <div class="col-12">
            <h2>Список всех покупателей:</h2>
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th>ID</th>
                    <th>Имя покупателя</th>
                    <th>Email</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <c:set var="customers" value='${customerDAO.getAll()}' />
                <c:forEach items="${customers}" var="customer">
                    <tr>
                        <th scope="row">${customer.id}</th>
                        <td>${customer.name}</td>
                        <td>${customer.email}</td>
                        <td class="buttons">
                            <form class="remove-form" method="post">
                                <input type="hidden" name="id" value="${customer.id}">
                                <button type="submit" name="customer-delete" class="btn btn-danger">Удалить</button>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
                <c:if test="${empty customers}">
                    <tr>
                        <th colspan="4" scope="row">ПОКУПАТЕЛЕЙ НЕТ</th>
                    </tr>
                </c:if>
                </tbody>
            </table>
        </div>
    </div>
</div>

<jsp:include page="/WEB-INF/views/admin/parts/footer.jsp"/>