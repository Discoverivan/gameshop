<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:include page="/WEB-INF/views/admin/parts/header.jsp"/>

<div class="container">
  <div class="row edit-list">
    <div class="col-12">
      <h2>Список всех заказов:</h2>
      <table class="table">
        <thead class="thead-dark">
          <tr>
            <th>ID</th>
            <th>Дата</th>
            <th>Имя покупателя</th>
            <th>Игры в заказе</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
        <c:set var="orders" value='${orderDAO.getAll()}' />
        <c:forEach items="${orders}" var="order">
        <tr>
          <th scope="row">${order.id}</th>
          <td>${order.date}</td>
          <td>${order.customer.name}</td>
          <td>
            <c:forEach items="${order.products}" var="product">
              ${product.game.title}<br>
            </c:forEach>
          </td>
          <td class="buttons">
            <form class="remove-form" method="post">
              <input type="hidden" name="id" value="${order.id}">
              <button type="submit" name="order-delete" class="btn btn-danger">Удалить</button>
            </form>
          </td>
        </tr>
        </c:forEach>
        <c:if test="${empty orders}">
        <tr>
          <th colspan="5" scope="row">ЗАКАЗОВ НЕТ</th>
        </tr>
        </c:if>
        </tbody>
      </table>
    </div>
  </div>
</div>

<jsp:include page="/WEB-INF/views/admin/parts/footer.jsp"/>
