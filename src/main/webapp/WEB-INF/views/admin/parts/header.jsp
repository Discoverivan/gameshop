<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="/WEB-INF/views/admin/parts/head-includes.jsp"/>
<body id="admin">
<header>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <a class="navbar-brand" href="/admin/">ПАНЕЛЬ УПРАВЛЕНИЯ</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/admin/game/">Игры</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/admin/order/">Заказы</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/admin/customer/">Покупатели</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/admin/stats/">Статистика</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Атрибуты</a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="/admin/genre/">Жанры</a>
                        <a class="dropdown-item" href="/admin/language/">Языки</a>
                        <a class="dropdown-item" href="/admin/platform/">Платформы</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Продукты</a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="/admin/product/list/">Список</a>
                        <a class="dropdown-item" href="/admin/product/import/">Импорт</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/admin/settings/">Настройки</a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/">Вернуться на сайт</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/logout">Выйти</a>
                </li>
            </ul>
        </div>
    </nav>
</header>