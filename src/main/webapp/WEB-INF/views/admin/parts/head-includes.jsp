<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta itemprop="image" content="/images/icon.png" />
    <link rel="icon" href="/images/icon.png" sizes="32x32" />
    <link rel="icon" href="/images/icon.png" sizes="192x192" />
    <link rel="apple-touch-icon-precomposed" href="/images/icon.png" />
    <meta name="msapplication-TileImage" content="/images/icon.png" />

    <meta name="viewport" content="width=device-width">
    <meta charset="utf-8">
    <title>Панель управления</title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css">
</head>
