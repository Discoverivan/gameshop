<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:include page="/WEB-INF/views/admin/parts/header.jsp"/>

<c:set var="games" value='${gameDAO.getAll()}' scope="session" />
<c:set var="pages" value="${gamePaginationService.getPages(games,25)}"/>
<c:set var="totalPages" value="${fn:length(pages)}"/>
<c:choose>
    <c:when test="${empty param.p}">
        <c:set var="currentPage" value="1"/>
    </c:when>
    <c:otherwise>
        <c:set var="currentPage" value="${param.p}"/>
        <c:choose>
            <c:when test="${currentPage > fn:length(pages)}">
                <c:set var="currentPage" value="${fn:length(pages)}"/>
            </c:when>
            <c:when test="${currentPage < 1}">
                <c:set var="currentPage" value="1"/>
            </c:when>
        </c:choose>
    </c:otherwise>
</c:choose>

<div id="game" class="container">
  <c:choose>
    <c:when test="${!empty editableGame}">
      <div class="row title" style="display: none;">
    </c:when>
    <c:otherwise>
      <div class="row title">
    </c:otherwise>
  </c:choose>
    <div class="col-6">
      <h2>Список всех игр:</h2>
    </div>
    <div class="col-6 add-btn">
      <button class="btn btn-primary" type="button" onclick="addGameInput(1)">Добавить игру</button>
    </div>
  </div>
  <c:choose>
  <c:when test="${!empty editableGame}">
      <div class="row edit-list" style="display: none;">
    </c:when>
    <c:otherwise>
      <div class="row edit-list">
    </c:otherwise>
  </c:choose>
    <div class="col-12">
        <table class="table">
        <thead class="thead-dark">
          <tr>
            <th>ID</th>
            <th>Title</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
        <c:choose>
            <c:when test="${!empty games}">
                <c:forEach items="${pages.get(currentPage-1)}" var="game">
                    <tr>
                        <th scope="row">${game.id}</th>
                        <td>${game.title}</td>
                        <td class="buttons">
                            <a class="btn btn-warning" href="/admin/game/edit/${game.id}">Редактировать</a>
                            <form class="remove-form" method="post" action="/admin/game/remove">
                                <input type="hidden" name="id" value="${game.id}">
                                <button type="submit" name="game-delete" class="btn btn-danger">Удалить</button>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
            </c:when>
            <c:otherwise>
                <tr>
                    <th colspan="3" scope="row"><% out.print("ИГР НЕТ"); %></th>
                </tr>
            </c:otherwise>
        </c:choose>
        </tbody>
        </table>

        <c:set var="pagesAround" value="2"/>
        <c:if test="${totalPages>1}">
        <div class="pages">
            <div class="pager">
                <ul class="pagination">
                    <c:if test="${(currentPage - pagesAround)>1}">
                        <li class="page-item">
                            <a class="page-link" href="?p=1">Первая</a>
                        </li>
                    </c:if>
                    <c:if test="${currentPage>1}">
                        <li class="page-item">
                            <a class="page-link" href="?p=${currentPage-1}">&laquo;</a>
                        </li>
                    </c:if>
                    <c:forEach begin="1" end="${totalPages}" varStatus="loop">
                        <c:choose>
                            <c:when test="${(loop.index >= (currentPage-pagesAround)) && (loop.index <= (currentPage+pagesAround))}">
                                <li class="page-item <c:if test="${currentPage == loop.index}">active</c:if>">
                                    <a class="page-link" href="?p=${loop.index}">${loop.index}</a>
                                </li>
                            </c:when>
                            <c:when test="${((currentPage+pagesAround) - loop.index) == 0}">
                                <li class="page-item <c:if test="${currentPage == loop.index}">active</c:if>">
                                    <a class="page-link" href="?p=${loop.index}">${loop.index}</a>
                                </li>
                            </c:when>
                        </c:choose>
                    </c:forEach>
                    <c:if test="${currentPage<totalPages}">
                        <li class="page-item">
                            <a class="page-link" href="?p=${currentPage+1}">&raquo;</a>
                        </li>
                    </c:if>
                    <c:if test="${(currentPage + pagesAround)<totalPages}">
                        <li class="page-item">
                            <a class="page-link" href="?p=${totalPages}">Последняя</a>
                        </li>
                    </c:if>
                </ul>
            </div>
        </div>
        </c:if>
    </div>
  </div>
    <c:choose>
      <c:when test="${!empty editableGame}">
      <div class="row game-input">
      </c:when>
      <c:otherwise>
        <div class="row game-input" style="display: none;">
      </c:otherwise>
    </c:choose>
    <div class="col-12">
      <c:choose>
        <c:when test="${!empty editableGame}">
          <h2>Редактирование игры:</h2>
        </c:when>
        <c:otherwise>
          <h2>Добавить игру:</h2>
        </c:otherwise>
      </c:choose>
      <form method="post">
        <c:if test="${!empty editableGame}">
          <input type="hidden" name="id" value="${editableGame.id}">
        </c:if>
        <div class="form-group">
          <label>Название: </label>
          <input type="text" class="form-control" name="title" value="${editableGame.title}" required>
        </div>
        <div class="form-group">
          <label>Описание: </label>
          <textarea class="form-control" name="description" rows="6" required>${editableGame.description}</textarea>
        </div>
        <c:set var="languages" value='${languageDAO.getAll()}' />
        <div class="form-group">
          <label>Поддерживаемые языки: </label>
          <select multiple class="form-control" name="languages" required>
            <c:forEach items="${languages}" var="language">
                <c:set var="finded" value="0" scope="page"/>
                <c:forEach items="${editableGame.languages}" var="t">
                    <c:if test="${t.id == language.id}">
                        <c:set var="finded" value="1"/>
                    </c:if>
                </c:forEach>
            <option
                    <c:if test="${finded == 1}">selected</c:if>
                    value="${language.id}">${language.name}</option>
            </c:forEach>
          </select>
        </div>
        <c:set var="platforms" value='${platformDAO.getAll()}' />
        <div class="form-group">
          <label>Поддерживаемые платформы: </label>
          <select multiple class="form-control" name="platforms" required>
            <c:forEach items="${platforms}" var="platform">
                <c:set var="finded" value="0" scope="page"/>
                <c:forEach items="${editableGame.platforms}" var="t">
                    <c:if test="${t.id == platform.id}">
                        <c:set var="finded" value="1"/>
                    </c:if>
                </c:forEach>
                <option
                        <c:if test="${finded == 1}">selected</c:if>
                        value="${platform.id}">${platform.name}</option>
            </c:forEach>
          </select>
        </div>
        <c:set var="genres" value='${genreDAO.getAll()}' />
        <div class="form-group">
          <label>Жанры: </label>
          <select multiple class="form-control" name="genres" required>
            <c:forEach items="${genres}" var="genre">
                <c:set var="finded" value="0" scope="page"/>
                <c:forEach items="${editableGame.genres}" var="t">
                    <c:if test="${t.id == genre.id}">
                        <c:set var="finded" value="1"/>
                    </c:if>
                </c:forEach>
              <option
                      <c:if test="${finded == 1}">selected</c:if>
                      value="${genre.id}">${genre.name}</option>
            </c:forEach>
          </select>
        </div>
        <div class="form-group">
          <label>Разработчик: </label>
          <input type="text" class="form-control" name="developer" value="${editableGame.developer}" required>
        </div>
        <div class="form-group">
          <label>Дата релиза игры: </label>
          <input type="date" class="form-control" name="release-date" value="${editableGame.releaseDate}" required>
        </div>
        <div class="form-group">
          <label>Ссылка на постер: </label>
          <input type="text" class="form-control" name="poster-image-url" value="${editableGame.posterImageUrl}" required>
        </div>
        <div class="form-group screenshots">
          <label>Сслыки на скриншоты: </label>
          <c:choose>
            <c:when test="${!empty editableGame}">
              <c:set var="i" value="1" scope="page"/>
              <div class="inputs">
              <c:forEach items="${editableGame.screenshotsUrls}" var="screenshot">
                <input type="text" class="form-control" name="screenshot" value="${screenshot}" required>
                <c:set var="i" value="${i + 1}" scope="page"/>
              </c:forEach>
              </div>
            </c:when>
            <c:otherwise>
              <div class="inputs">
                <input type="text" class="form-control" name="screenshot" required>
              </div>
            </c:otherwise>
          </c:choose>
          <button type="button" name="addScreen" class="btn btn-primary btn-sm" onclick="addScreenshotField()">Добавить поле</button>
          <button type="button" name="removeScreen" class="btn btn-danger btn-sm" disabled onclick="removeScreenshotField()">Удалить поле</button>
        </div>
        <div class="form-group">
          <label>Цена игры: </label>
          <input type="number" min="0" class="form-control" name="price" value="${editableGame.price}" required>
        </div>
        <c:choose>
          <c:when test="${!empty editableGame}">
            <button type="submit" name="game-edit-submit" class="btn btn-primary">Завершить редактирование</button>
            <a class="btn btn-secondary" href="/admin/game/">Назад</a>
          </c:when>
          <c:otherwise>
            <button type="submit" name="game-add-submit" class="btn btn-primary">Добавить игру</button>
            <button class="btn btn-secondary" type="button" onclick="addGameInput(0)">Закрыть</button>
          </c:otherwise>
        </c:choose>
      </form>
    </div>
  </div>
</div>

<jsp:include page="/WEB-INF/views/admin/parts/footer.jsp"/>
