<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:set var="editableGame" value='${gameDAO.get(urlParam1)}' scope="request" />

<jsp:include page="/WEB-INF/views/admin/game/game.jsp"/>
