<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:include page="/WEB-INF/views/admin/parts/header.jsp"/>

<div class="container">
    <div class="row edit-list">
      <div class="col-12">
        <h2>Список всех языков:</h2>
        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th>ID</th>
              <th>Название</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
          <c:set var="languages" value='${languageDAO.getAll()}' />
          <c:forEach items="${languages}" var="language">
          <tr>
            <th scope="row">${language.id}</th>
            <td>${language.name}</td>
            <td class="buttons">
              <a class="btn btn-warning" href="/admin/language/edit/${language.id}">Редактировать</a>
              <form class="remove-form" method="post" action="/admin/language/remove">
                <input type="hidden" name="id" value="${language.id}">
                <button type="submit" name="language-delete" class="btn btn-danger">Удалить</button>
              </form>
            </td>
          </tr>
          </c:forEach>
          <c:if test="${empty languages}">
          <tr>
            <th colspan="3" scope="row"><% out.print("ПОКА НИЧЕГО НЕ ЗАПОЛНЕНО"); %></th>
          </tr>
          </c:if>
          </tbody>
        </table>
      </div>
    </div>
    <div class="row edit-list">
      <div class="col-12">
        <h2>Добавить язык:</h2>
        <form method="post">
          <div class="form-group">
            <label>Название: </label>
            <input type="text" class="form-control" name="name" required>
          </div>
          <button type="submit" name="language-add-submit" class="btn btn-primary">Добавить язык</button>
        </form>
      </div>
    </div>
</div>

<jsp:include page="/WEB-INF/views/admin/parts/footer.jsp"/>
