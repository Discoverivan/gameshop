initialize();

function initialize() {
    if ($("#admin-settings").length) {
        var options = {
            beforeSubmit: addNewAdminBefore,
            success: addNewAdminSuccess,
            data_type: 'json'
        };
        $('#admin-settings .add-admin').ajaxForm(options);

        options = {
            beforeSubmit: deleteAdminBefore,
            success: deleteAdminSuccess,
            data_type: 'json'
        };
        $('#admin-settings .remove-admin').ajaxForm(options);
    }
    if ($("#game").length) {
        checkScreenFieldButtons();
    }
    if ($("#admin-product-import").length) {
        var options = {
            beforeSubmit: productImportBefore,
            success: productImportSuccess,
            error: productImportError,
            data_type: 'json'
        };
        $('#admin-product-import .external-form').ajaxForm(options);
    }

    if ($("#admin-product-list").length) {
        var options = {
            beforeSubmit: productListBefore,
            success: productListSuccess,
            error: productListError,
            data_type: 'json'
        };
        $('#admin-product-list .external-form').ajaxForm(options);
    }
    if ($("#admin-stats").length) {
        loadStats();
    }
}

function addNewAdminBefore(formData, jqForm, options) {
    $("#admin-settings .alert-danger").slideUp("fast");
    $("#admin-settings .alert-success").slideUp("fast");
    $("#admin-settings .add-admin button").attr("disabled", "disabled");
}

function addNewAdminSuccess(responseText, statusText, xhr, $form) {
    $("#admin-settings .add-admin button").removeAttr("disabled");
    if (responseText === "0"){
        $("#admin-settings .alert-danger").slideDown("fast");
    }
    if (responseText === "1"){
        $("#admin-settings .alert-success").slideDown("fast");
        location.reload();
    }
}

function deleteAdminBefore() {
    $("#admin-settings .admin-list .alert").slideUp("fast");
}

function deleteAdminSuccess(responseText, statusText, xhr, $form) {
    if (responseText === "1"){
        $form.parent().parent().slideUp("fast");
    } else {
        $("#admin-settings .admin-list .alert").slideDown("fast");
    }
}

function checkScreenFieldButtons() {
    $(".screenshots button[name='addScreen']").attr("disabled", false);
    $(".screenshots button[name='removeScreen']").attr("disabled", false);
    if (getScreenCount() >= 4){
        $(".screenshots button[name='addScreen']").attr("disabled", true);
    }
    if (getScreenCount() <= 1){
        $(".screenshots button[name='removeScreen']").attr("disabled", true);
    }
}

function getScreenCount() {
    return $(".screenshots .inputs :input").length;
}

function addScreenshotField() {
    if (getScreenCount() < 4) {
        var input =
            '<input type="text" ' +
                'class="form-control" ' +
                'name="screenshot" required>';
        $(".screenshots .inputs").append(input);
        checkScreenFieldButtons();
    }
}

function removeScreenshotField() {
    if (getScreenCount() > 1) {
        $(".screenshots .inputs input[name='screenshot']").remove();
        checkScreenFieldButtons();
    }
}

function addGameInput(open) {
    if (open === 1){
        $("#game .title").slideUp("fast");
        $("#game .edit-list").slideUp("fast");
        $("#game .game-input").slideDown("fast");
    } else {
        $("#game .title").slideDown("fast");
        $("#game .edit-list").slideDown("fast");
        $("#game .game-input").slideUp("fast");
    }
}

function productImportBefore() {
    $("#admin-product-import .alert").slideUp();
    $('#admin-product-import .external-form').attr("disabled", true);
}
function productImportSuccess(responseText) {
    console.log(responseText);
    $("#admin-product-import .alert").slideDown().html("Добавлено в базу <b>"+responseText+"</b> ключей с сервера поставщика");
    $('#admin-product-import .external-form').attr("disabled", false);
}

function productImportError() {
    $('#admin-product-import .external-form').attr("disabled", false);
}

function productListBefore() {
    $("#admin-product-list .alert").slideUp();
    $('#admin-product-list .external-form').attr("disabled", true);
}
function productListSuccess(responseText) {
    console.log(responseText);
    $("#admin-product-list .alert").slideDown().html("Получено <b>"+responseText.length+"</b> ключей");
    $('#admin-product-list .external-form').attr("disabled", false);
}

function productListError() {
    $('#admin-product-list .external-form').attr("disabled", false);
}

//STATS
var loadFullStatsAttempts = 0;
var loadShortStatsAttempts = 0;

function loadStats() {
    loadShortStats();
    loadFullStats();
}

function loadShortStats() {
    $.ajax({
        type: "POST",
        url: "short",
        data: "",
        success: loadShortStatsSuccess,
        error: loadShortStatsError,
        dataType: 'json'
    });
}

function loadShortStatsSuccess(data) {
    $("#all-games .number").html(data['totalGames']);
    $("#all-orders .number").html(data['totalOrders']);
    $("#all-customers .number").html(data['totalCustomers']);
    $("#load-short-stats").slideUp('fast');
    $("#short-stats-block").slideDown('fast');
    loadShortStatsAttempts = 0;
}

function loadShortStatsError() {
    if (loadShortStatsAttempts < 3) {
        loadShortStatsAttempts++;
        loadShortStats();
    }
}

function changeTimeIntervalOnFullStats() {
    $("#load-full-stats").slideDown('fast');
    $("#full-stats-block").slideUp('fast');
    loadFullStats();
}

function loadFullStats() {
    var timeInterval = $("#time-interval").val();
    $("#time-interval").attr("disabled", true);
    $.ajax({
        type: "POST",
        url: "full",
        data: "time-interval="+timeInterval,
        success: loadFullStatsSuccess,
        error: loadFullStatsError,
        dataType: 'json'
    });
}

function loadFullStatsSuccess(data) {
    $("#orders-count .number").html(data['ordersCount']);
    $("#orders-sum .number").html(data['ordersSum']+" ₽");
    loadFullDataGraphs(data);

    console.log(data);

    $("#time-interval").attr("disabled", false);
    $("#load-full-stats").slideUp('fast');
    $("#full-stats-block").slideDown('fast');

    loadFullStatsAttempts = 0;
}


function loadFullStatsError() {
    if (loadFullStatsAttempts < 3) {
        loadFullStatsAttempts++;
        loadFullStats();
    }
}

function loadFullDataGraphs(data) {
    var ctx = $("#ordersGraph");
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: data["ordersGraph"]["labels"],
            datasets: [{
                label: 'Заказы',
                data: data["ordersGraph"]["data"],
                borderWidth: 2,
                backgroundColor: 'rgba(255, 255, 255, 0)',
                borderColor: '#f47318'
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
}
