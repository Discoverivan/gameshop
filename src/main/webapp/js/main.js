initialize();
function initialize() {
    if ($("#index").length) {
        $('#banners-slider').carousel({
            interval: 7000
        })
    }
    if ($("#register").length) {
        var options = {
            beforeSubmit: registerNewCustomerBefore,
            success: registerNewCustomerSuccess,
            error: registerNewCustomerError,
            data_type: 'json'
        };
        $('#register .register-form').ajaxForm(options);
    }
}

function registerNewCustomerBefore(formData, jqForm, options) {
    $("#register .register-form .alert").slideUp("fast");
    $("#register .register-form button").attr("disabled", "disabled");
    $("#register .register-form button").html("Подождите");
}

function registerNewCustomerSuccess(responseText, statusText, xhr, $form) {
    $("#register .register-form button").removeAttr("disabled");
    $("#register .register-form button").html("Создать аккаунт");
    if (responseText.success){
        document.location.href="/";
        return;
    }
    formShowValidationErrors($("#register .register-form"), responseText.fieldErrors);
}

function registerNewCustomerError() {
    $("#register .register-form .alert").slideDown("fast");
    $("#register .register-form button").removeAttr("disabled");
    $("#register .register-form button").html("Создать аккаунт");
}

//VALIDATION
function formShowValidationErrors(form, errors) {
    form.find('input').tooltip('hide');
    form.find('input').removeClass("is-invalid");
    for (var k in errors){
        if (errors.hasOwnProperty(k)) {
            var inputObj = form.find('input[name='+k+']');
            inputObj.attr("data-original-title", errors[k]);
            inputObj.tooltip('show');
            inputObj.addClass("is-invalid");
        }
    }
}
